{
  "openapi": "3.0.1",
  "info": {
    "title": "caos-WEB API",
    "description": "Web API for managing Creatures 3 / Docking Station",
    "contact": {
      "name": "Gitlab Project",
      "url": "https://gitlab.com/station96/creatures-enlightenment-ressources"
    },
    "license": {
      "name": "This project is licensed under the GNU General Public License v3.0",
      "url": "https://gitlab.com/station96/creatures-enlightenment-ressources/-/blob/ce22ab1b50e4749fa855b3ada33f434feb39edf7/LICENSE"
    },
    "version": "v1"
  },
  "paths": {
    "/Creatures": {
      "get": {
        "tags": [
          "Creature"
        ],
        "summary": "List all Creatures (Norn, Grendel, Ettin, Geat) in the current World",
        "description": "Sample request:\r\n            \r\n    GET /Creature/",
        "responses": {
          "200": {
            "description": "Return Creatures (moniker, name, breed, gender)",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Creature"
                  }
                }
              }
            }
          },
          "204": {
            "description": "No Creature found in current world"
          },
          "500": {
            "description": "Internal Server Error"
          }
        }
      }
    },
    "/Creature/{moniker}": {
      "get": {
        "tags": [
          "Creature"
        ],
        "summary": "Show full info of the Creature with the given Moniker",
        "description": "Sample request:\r\n            \r\n    GET /Creature/072-twig-8tj7b-2te5v-6gunb-vvj6y",
        "parameters": [
          {
            "name": "moniker",
            "in": "path",
            "description": "Creature's Moniker",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Returns the Creature with the specified Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Creature"
                }
              }
            }
          },
          "400": {
            "description": "Not a valid Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProblemDetails"
                }
              }
            }
          },
          "404": {
            "description": "Creature not found with the specified Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProblemDetails"
                }
              }
            }
          },
          "500": {
            "description": "Internal Server Error"
          }
        }
      }
    },
    "/Creature/{moniker}/LifeEvent": {
      "get": {
        "tags": [
          "Creature"
        ],
        "summary": "Show the Creature's Life Event with the given Moniker",
        "description": "Sample request:\r\n            \r\n    GET /Creature/072-twig-8tj7b-2te5v-6gunb-vvj6y/LifeEvent",
        "parameters": [
          {
            "name": "moniker",
            "in": "path",
            "description": "Creature's Moniker",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Returns the Creature's Life Event with the specified Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/LifeEvents"
                  }
                }
              }
            }
          },
          "400": {
            "description": "Not a valid Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProblemDetails"
                }
              }
            }
          },
          "404": {
            "description": "Creature not found with the specified Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProblemDetails"
                }
              }
            }
          },
          "500": {
            "description": "Internal Server Error"
          }
        }
      }
    },
    "/Creature/{moniker}/Chemical": {
      "get": {
        "tags": [
          "Creature"
        ],
        "summary": "Show the Creature's Chemicals rate with the given Moniker",
        "description": "Sample request:\r\n            \r\n    GET /Creature/072-twig-8tj7b-2te5v-6gunb-vvj6y/Chemical",
        "parameters": [
          {
            "name": "moniker",
            "in": "path",
            "description": "Creature's Moniker",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Returns the Creature's Chemicals rate with the specified Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/ChemicalsProduct"
                  }
                }
              }
            }
          },
          "400": {
            "description": "Not a valid Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProblemDetails"
                }
              }
            }
          },
          "404": {
            "description": "Creature not found with the specified Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProblemDetails"
                }
              }
            }
          },
          "500": {
            "description": "Internal Server Error"
          }
        }
      }
    },
    "/Creature/{moniker}/Organ": {
      "get": {
        "tags": [
          "Creature"
        ],
        "summary": "Show the Creature's Organs rate with the given Moniker",
        "description": "Sample request:\r\n            \r\n    GET /Creature/072-twig-8tj7b-2te5v-6gunb-vvj6y/Organ",
        "parameters": [
          {
            "name": "moniker",
            "in": "path",
            "description": "Creature's Moniker",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Returns the Creature's Organ rate with the specified Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/CreatureOrgan"
                  }
                }
              }
            }
          },
          "400": {
            "description": "Not a valid Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProblemDetails"
                }
              }
            }
          },
          "404": {
            "description": "Creature not found with the specified Moniker",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProblemDetails"
                }
              }
            }
          },
          "500": {
            "description": "Internal Server Error"
          }
        }
      }
    },
    "/Creatures/LearnVocabulary": {
      "put": {
        "tags": [
          "Creature"
        ],
        "summary": "Learn vocabulary to all Creatures of the world",
        "description": "Sample request:\r\n            \r\n    PUT /Creature/LearnVocabulary",
        "responses": {
          "200": {
            "description": "All Creatures have been teached !",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "500": {
            "description": "Internal Error"
          }
        }
      }
    },
    "/Creature/{moniker}/FocusCreature": {
      "put": {
        "tags": [
          "Creature"
        ],
        "summary": "Select and show in-game the Creature's given moniker.",
        "description": "Sample request:\r\n            \r\n    PUT /Creature/FocusCreature",
        "parameters": [
          {
            "name": "moniker",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Creature has been selected !",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "500": {
            "description": "Internal Error"
          }
        }
      }
    },
    "/Creatures/AgeAdultAll": {
      "put": {
        "tags": [
          "Creature"
        ],
        "summary": "Age to Adult all Creatures of the world",
        "description": "Sample request:\r\n            \r\n    PUT /Creature/AgeAdultAll",
        "responses": {
          "200": {
            "description": "All Creatures have been Aged to Adult !",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "500": {
            "description": "Internal Error"
          }
        }
      }
    },
    "/Creatures/KillImmortalCreature": {
      "put": {
        "tags": [
          "Creature"
        ],
        "summary": "Kill Creatures with age >= threshold (in hours)",
        "description": "Sample request:\r\n            \r\n    PUT /Creature/KillImmortalCreature?Threshold=24",
        "parameters": [
          {
            "name": "Threshold",
            "in": "query",
            "description": "Creatures ages equal or than Threshold (in hours) are killed",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int32"
            },
            "example": 24
          }
        ],
        "responses": {
          "200": {
            "description": "Immortal Creatures killed !",
            "content": {
              "application/json": {
                "schema": {
                  "type": "integer",
                  "format": "int32"
                }
              }
            }
          },
          "500": {
            "description": "Internal Error"
          }
        }
      }
    },
    "/World/GameVariable": {
      "get": {
        "tags": [
          "World"
        ],
        "summary": "List all GAME variable",
        "description": "Sample request:\r\n            \r\n    GET /World/GetGameVariable\r\n            \r\nSome Game Variable's value are marked as \"Unknown\" when her values can't be interpreted.",
        "responses": {
          "200": {
            "description": "A table of GAME variable",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/CaosGameVariable"
                  }
                }
              }
            }
          },
          "500": {
            "description": "Internal Error"
          }
        }
      }
    },
    "/World/Ecology": {
      "get": {
        "tags": [
          "World"
        ],
        "summary": "List all Species from current world",
        "description": "Sample request:\r\n            \r\n    GET /World/Ecology",
        "responses": {
          "200": {
            "description": "A table of Specie object",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Specie"
                  }
                }
              }
            }
          },
          "500": {
            "description": "Internal Error"
          },
          "204": {
            "description": "No Specie found in current world"
          }
        }
      }
    },
    "/World/ResetEcology": {
      "get": {
        "tags": [
          "World"
        ],
        "summary": "Reset all Species from current world",
        "description": "Sample request:\r\n            \r\n    PUT /World/ResetEcology",
        "responses": {
          "200": {
            "description": "World's Ecology have been reseted !",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "500": {
            "description": "Internal Error"
          }
        }
      }
    },
    "/World/InjectAgentFromFileCaos": {
      "put": {
        "tags": [
          "World"
        ],
        "summary": "Inject Agent from CAOS file from Bootstrap Folder",
        "description": "Sample request:\r\n            \r\n    PUT /World/InjectAgentFromFileCaos?Agent=hoverdoc",
        "parameters": [
          {
            "name": "Agent",
            "in": "query",
            "description": "CAOS Filename without .cos extension",
            "required": true,
            "schema": {
              "type": "string"
            },
            "example": "hoverdoc"
          }
        ],
        "responses": {
          "200": {
            "description": "Agent injected !",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "500": {
            "description": "Internal Error"
          }
        }
      }
    },
    "/World/EggsToMuco": {
      "put": {
        "tags": [
          "World"
        ],
        "summary": "Move all Eggs near Muco in Capillata's Norn Meso",
        "description": "Sample request:\r\n            \r\n    GET /World/MoveEggsMuco",
        "responses": {
          "200": {
            "description": "Eggs have been moved",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "500": {
            "description": "Internal Server Error"
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Area": {
        "enum": [
          "Norn_Terrarium",
          "Grendel_Jungle",
          "Ettin_Desert",
          "Aquarium"
        ],
        "type": "string"
      },
      "AssociatedMonikerType": {
        "enum": [
          "blank",
          "Mother",
          "Father",
          "Spliced_From_1",
          "Spliced_From_2",
          "Child",
          "Engineered_From",
          "Cloned_From",
          "Cloned_To"
        ],
        "type": "string"
      },
      "Breed": {
        "enum": [
          "Norn",
          "Grendel",
          "Ettin",
          "Geat"
        ],
        "type": "string"
      },
      "CaosGameVariable": {
        "type": "object",
        "properties": {
          "number": {
            "type": "integer",
            "format": "int32"
          },
          "name": {
            "type": "string",
            "nullable": true
          },
          "value": {
            "type": "string",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "ChemicalsProduct": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "name_EN": {
            "type": "string",
            "nullable": true
          },
          "name_FR": {
            "type": "string",
            "nullable": true
          },
          "description_EN": {
            "type": "string",
            "nullable": true
          },
          "description_FR": {
            "type": "string",
            "nullable": true
          },
          "value": {
            "type": "number",
            "format": "double"
          }
        },
        "additionalProperties": false
      },
      "Creature": {
        "type": "object",
        "properties": {
          "lastRefreshDate": {
            "type": "string",
            "format": "date-time",
            "nullable": true
          },
          "moniker": {
            "type": "string",
            "nullable": true
          },
          "name": {
            "type": "string",
            "nullable": true
          },
          "birthday": {
            "type": "string",
            "nullable": true,
            "readOnly": true
          },
          "age": {
            "type": "string",
            "nullable": true
          },
          "currentLifeStage": {
            "$ref": "#/components/schemas/LifeStage"
          },
          "firstParentMoniker": {
            "type": "string",
            "nullable": true,
            "readOnly": true
          },
          "secondParentMoniker": {
            "type": "string",
            "nullable": true,
            "readOnly": true
          },
          "breed": {
            "$ref": "#/components/schemas/Breed"
          },
          "generation": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "gender": {
            "$ref": "#/components/schemas/Sex"
          },
          "mutationsNumber": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "crossOverPoint": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "tint_Red": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "tint_Green": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "tint_Blue": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "tint_Rotation": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "tint_Swap": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "worldOfBirth": {
            "type": "string",
            "nullable": true
          },
          "currentMonikerState": {
            "$ref": "#/components/schemas/MonikerState"
          },
          "organCount": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "CreatureOrgan": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "organ": {
            "type": "string",
            "nullable": true
          },
          "description": {
            "type": "string",
            "nullable": true
          },
          "value": {
            "type": "number",
            "format": "double",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "LifeEvents": {
        "type": "object",
        "properties": {
          "lifeEventRef": {
            "$ref": "#/components/schemas/LifeEventsReference"
          },
          "description": {
            "type": "string",
            "nullable": true
          },
          "creatureAge": {
            "$ref": "#/components/schemas/TimeSpan"
          },
          "eventDate": {
            "type": "string",
            "format": "date-time"
          },
          "associated_Moniker_1_Type": {
            "$ref": "#/components/schemas/AssociatedMonikerType"
          },
          "associated_Moniker_1": {
            "type": "string",
            "nullable": true
          },
          "associated_Moniker_2_Type": {
            "$ref": "#/components/schemas/AssociatedMonikerType"
          },
          "associated_Moniker_2": {
            "type": "string",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "LifeEventsReference": {
        "enum": [
          "Conceived_0",
          "Spliced_1",
          "Engineered_2",
          "Born_3",
          "Aged_4",
          "Exported_5",
          "Imported_6",
          "Died_7",
          "BecamePregnant_8",
          "Impregnated_9",
          "ChildBorn_10",
          "LaidByMother_11",
          "LaidAnEgg_12",
          "Photographed_13",
          "Cloned_14",
          "CloneSource_15",
          "WarpedOut_16",
          "WarpedIn_17"
        ],
        "type": "string"
      },
      "LifeStage": {
        "enum": [
          "Baby",
          "Child",
          "Adolescent",
          "Youth",
          "Adult",
          "Old",
          "Ancient",
          "Death"
        ],
        "type": "string"
      },
      "MonikerState": {
        "enum": [
          "NeverExistedOrPurged",
          "SlotReferenceEgg",
          "MadeWithNewCrea",
          "ProperlyBorn",
          "Exported",
          "DeadWithBody",
          "DeadWithoutBody",
          "Unreferenced"
        ],
        "type": "string"
      },
      "ProblemDetails": {
        "type": "object",
        "properties": {
          "type": {
            "type": "string",
            "nullable": true
          },
          "title": {
            "type": "string",
            "nullable": true
          },
          "status": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "detail": {
            "type": "string",
            "nullable": true
          },
          "instance": {
            "type": "string",
            "nullable": true
          }
        },
        "additionalProperties": { }
      },
      "Sex": {
        "enum": [
          "Male",
          "Female",
          "Other",
          "Egg"
        ],
        "type": "string"
      },
      "Specie": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "nullable": true
          },
          "latinName": {
            "type": "string",
            "nullable": true
          },
          "classification": {
            "type": "string",
            "nullable": true
          },
          "niche": {
            "type": "string",
            "nullable": true
          },
          "caosFile": {
            "type": "string",
            "nullable": true
          },
          "quantity": {
            "type": "integer",
            "format": "int32"
          },
          "originPlace": {
            "$ref": "#/components/schemas/Area"
          }
        },
        "additionalProperties": false
      },
      "TimeSpan": {
        "type": "object",
        "properties": {
          "ticks": {
            "type": "integer",
            "format": "int64"
          },
          "days": {
            "type": "integer",
            "format": "int32"
          },
          "hours": {
            "type": "integer",
            "format": "int32"
          },
          "milliseconds": {
            "type": "integer",
            "format": "int32"
          },
          "minutes": {
            "type": "integer",
            "format": "int32"
          },
          "seconds": {
            "type": "integer",
            "format": "int32"
          },
          "totalDays": {
            "type": "number",
            "format": "double",
            "readOnly": true
          },
          "totalHours": {
            "type": "number",
            "format": "double",
            "readOnly": true
          },
          "totalMilliseconds": {
            "type": "number",
            "format": "double",
            "readOnly": true
          },
          "totalMinutes": {
            "type": "number",
            "format": "double",
            "readOnly": true
          },
          "totalSeconds": {
            "type": "number",
            "format": "double",
            "readOnly": true
          }
        },
        "additionalProperties": false
      }
    }
  }
}