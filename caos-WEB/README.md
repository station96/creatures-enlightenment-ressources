﻿# caos-WEB

A WEB server to connect Docking Station and view informations about World, Creatures, Agents, etc...

Swagger documentation [here !](https://gitlab.com/station96/creatures-enlightenment-ressources/-/blob/d74bcd763fa3ca399cc3d81213075dd24f4d91ce/swagger.json) (swagger.json)

**Please be sure Docking Station is lauched before executing**.

![Welcome Screen](/assets/images/caos-WEB-001.png)

Please be kind : this is my first API  Web Project in C# : code is ugly but i'm learning ;-)

## Latest features

- Kill Immortal Norn : you can kill all Creatures ages >= threshold you specified
- Swagger revamp ;-)

## Built with

- Love ;-)
- caos-CLI my first Project ([repository here !](https://gitlab.com/station96/creatures-enlightenment-ressources/-/tree/main/caos-CLI))
- **CAOS.dll** from [AlbianWarp/Caos-Class-Library](https://github.com/AlbianWarp/Caos-Class-Library).
- Microsoft Visual Studio Community 2022 and .Net6 Framework
- Windows 10

## Requirements

- .NET 6.0 Runtime [here](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime)
- Docking Station of course : available for free [here](https://store.steampowered.com/app/1659050/Creatures_Docking_Station/)

## Installation

- Clone repo or download at least the directory ```\caos-CLI\bin\```
- Unzip anywhere on you computer
- Run Docking Station and you world
- Run ```caos-WEB.exe```

## Usage

- API Documentation (Swagger) is available at ```https://127.0.0.1:5001/swagger/index.html```
- URL Base API is ```https://127.0.0.1:5001```

You can :

- List all Creatures (Norn, Grendel, Ettin, Geat) in the current World
- Show full info of the Creature with the given Moniker
- Show the Creature's Life Event with the given Moniker
- Show the Creature's Chemicals rate with the given Moniker
- Show the Creature's Organs rate with the given Moniker
- Learn vocabulary to all Creatures of the world
- Select and show in-game the Creature's given moniker
- Age to Adult all Creatures of the world
- Kill all Creatures ages >= threshold you specified (NEW !)
- Move all Eggs near Muco in Capillata's Norn Meso
- List all Species from current world
- Reset all Species from current world
- Inject Agent from CAOS file from Bootstrap Folder
- List all GAME variable

![](/assets/images/caos-WEB-002.png)

![](/assets/images/caos-WEB-003.png)

![](/assets/images/caos-CLI-WEB.png)

## Help

If you have a message "**Your connection is not private**" in web browser, follow those steps :

Clic on ```Advanced``` button :

![](/assets/images/caos-WEB-007.png)

And on ```Proceed to localhost``` :

![](/assets/images/caos-WEB-008.png)

This happen because there is no SSL certificate on a localhost connection.

## Contributing

Any contributions you could make are greatly appreciated.

## License

Distributed under the GNU General Public License v3.0. See ``LICENSE`` in main directory for more information.
