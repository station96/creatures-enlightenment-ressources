using System;
using CAOS;

namespace caos_WEB.Models;

public class Specie
{
    public CaosInjector injector = new CaosInjector("Docking Station");
    public string Name { get; set; } = "";
    public string LatinName { get; set; } = "";
    public string Classification { get; set; } = "";
    public string Niche { get; set; } = "";
    public string CaosFile { get; set; } = "";
    public int Quantity { get; set; } = 0;
    public Area OriginPlace { get; set; }

    public Specie(string _name, string _latinName, string _classification, string _niche, string _caosFile, int _area)
    {
        Name = _name;
        LatinName = _latinName;
        Classification = _classification;
        Niche = _niche;
        CaosFile = _caosFile;
        Quantity = Count();
        OriginPlace = (Area)_area;
    }

    public int Count()
    {
        injector.TryExecuteCaos($"outv totl {Classification}", out CaosResult res);
        return Convert.ToInt32(res.Content.Trim('\0'));
    }

    public int ReInject()
    {
        string req = $"ject \"{CaosFile}\" 7";
        injector.TryExecuteCaos(req, out CaosResult result);

        if (result.Success)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}

public enum Area
{
    Norn_Terrarium = 1,
    Grendel_Jungle = 2,
    Ettin_Desert = 3,
    Aquarium = 4
}
