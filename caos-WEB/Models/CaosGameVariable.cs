﻿namespace caos_WEB.Models
{
    public class CaosGameVariable
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public CaosGameVariable(int number, string name, string value)
        {
            this.Number = number;
            this.Name = name;
            this.Value = value;
        }
    }
}
