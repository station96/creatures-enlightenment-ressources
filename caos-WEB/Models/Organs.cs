using System;
using System.Text.Json;
using System.Collections.Generic;

namespace caos_WEB.Models;

public class CreatureOrgan
{
    public int? Id { get; set; }
    public string? Organ { get; set; }
    public string? Description { get; set; }
    public double? Value { get; set; }

    public static double ConversionTauxOrgan(string sVal, char Separator, long Precision)
    {
        string[] sDouble;
        double Val;

        sDouble = sVal.Split(new char[1] { Separator });
        Val = Convert.ToInt64(sDouble[0]) * Precision + Convert.ToInt64(sDouble[1]);
        return Val / Precision;
    }
}