using System;
using System.Text.Json;
using System.Collections.Generic;

namespace caos_WEB.Models;

public class ChemicalsProduct
{
    public int? Id { get; set; }
    public string? Name_EN { get; set; }
    public string? Name_FR { get; set; }
    public string? Description_EN { get; set; }
    public string? Description_FR { get; set; }
    public double Value { get; set; }

    public static double ConversionTauxChem(string sVal, char Separator, long Precision)
    {
        string[] sDouble;
        double Val;

        sDouble = sVal.Split(new char[1] { Separator });
        Val = Convert.ToInt64(sDouble[0]) * Precision + Convert.ToInt64(sDouble[1]);
        return Val / Precision;
    }
}
