using System;

namespace caos_WEB.Models;

public class LifeEvents
{
    public enum LifeEventsReference
    {
        Conceived_0 = 0, // a natural start to life, associated monikers are the mother's and father's
        Spliced_1 = 1, // created using GENE CROS to crossover the two associated monikers
        Engineered_2 = 2, // from a human made genome with GENE LOAD, the first associated moniker is blank, and the second is the filename
        Cloned_14 = 14, // such as when importing a creature that already exists in the world and reallocating the new moniker, when TWINing or GENE CLONing; associated moniker is who we were cloned from
        Born_3 = 3, // triggered by the BORN command, associated monikers are the parents.
        Aged_4 = 4, // reached the next life stage, either naturally from the ageing loci or with AGES
        Exported_5 = 5, // emmigrated to another world
        Imported_6 = 6, // immigrated back again
        Died_7 = 7, // triggered naturally with the death trigger locus, or by the DEAD command
        BecamePregnant_8 = 8, // the first associated moniker is the child, and the second the father
        Impregnated_9 = 9, // first associated moniker is the child, second the mother
        ChildBorn_10 = 10, // first moniker is the child, second the other parent
        CloneSource_15 = 15, // someone was cloned from you, first moniker is whom
        WarpedOut_16 = 16, // exported through a worm hole with NET: EXPO
        WarpedIn_17 = 17, // imported through a worm hole
        LaidByMother_11 = 11, // These events aren't triggered by the engine, but reserved for CAOS to use with these numbers
        LaidAnEgg_12 = 12, // These events aren't triggered by the engine, but reserved for CAOS to use with these numbers
        Photographed_13 = 13 // These events aren't triggered by the engine, but reserved for CAOS to use with these numbers
    }
    public enum AssociatedMonikerType
    {
        blank = 0, // from a human made genome with GENE LOAD, the first associated moniker is blank, and the second is the filename
        Mother = 1, // a natural start to life, associated monikers are the mother's and father's
        Father = 2, // a natural start to life, associated monikers are the mother's and father's
        Spliced_From_1 = 3, // created using GENE CROS to crossover the two associated monikers
        Spliced_From_2 = 4, // created using GENE CROS to crossover the two associated monikers
        Child = 5, // Became pregnant and Impregnated : first associated moniker is the child
        Engineered_From = 6, // from a human made genome with GENE LOAD, the first associated moniker is blank, and the second is the filename
        Cloned_From = 7, // such as when importing a creature that already exists in the world and reallocating the new moniker, when TWINing or GENE CLONing; associated moniker is who we were cloned from
        Cloned_To = 8 // someone was cloned from you, first moniker is whom
    }

    public LifeEventsReference LifeEventRef { get; set; }
    public string Description { get; set; } = "";
    public TimeSpan CreatureAge { get; set; }
    public DateTimeOffset EventDate { get; set; }
    public AssociatedMonikerType Associated_Moniker_1_Type { get; set; }
    public string Associated_Moniker_1 { get; set; } = "";
    public AssociatedMonikerType Associated_Moniker_2_Type { get; set; }
    public string Associated_Moniker_2 { get; set; } = "";

    public LifeEvents()
    {
        //
    }

    public LifeEvents(int _LifeEventRef, TimeSpan _CreatureAge, DateTimeOffset _EventDate, int _type_mon1, string _mon1, int _type_mon2, string _mon2)
    {
        LifeEventRef = (LifeEventsReference)_LifeEventRef;
        CreatureAge = _CreatureAge;
        EventDate = _EventDate;
        Associated_Moniker_1_Type = (AssociatedMonikerType)_type_mon1;
        Associated_Moniker_2_Type = (AssociatedMonikerType)_type_mon2;
        Associated_Moniker_1 = _mon1;
        Associated_Moniker_2 = _mon2;
    }

}
