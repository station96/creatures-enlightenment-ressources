﻿using caos_WEB.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace caos_WEB.Controllers;

[ApiController]
[Route("[controller]")]
[Produces("application/json")]
public class WorldController : ControllerBase
{
    private World _world;

    public WorldController()
    {
        _world = new World();
    }

    /// <summary>
    /// List all GAME variable
    /// </summary>
    /// <returns>A table of CaosGameVariable object where :
    ///     - "number" is Game's Variable number
    ///     - "name" is in-game Game Variable name
    ///     - "value" is in-game variable value
    /// if request id OK. Return "FAILED" if KO.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     GET /World/GetGameVariable
    ///
    /// Some Game Variable's value are marked as "Unknown" when her values can't be interpreted.
    /// </remarks>
    /// <response code="200">A table of GAME variable</response>
    /// <response code="500">Internal Error</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//World/[action]")]
    public ActionResult<List<CaosGameVariable>> GameVariable()
    {
        this._world.GetGameVariable();

        if (this._world.CurrentGameVariableList.Count() == 0)
        {
            return StatusCode(500, "FAILED");
        }

        return Ok(this._world.CurrentGameVariableList.ToList());
    }

    /// <summary>
    /// List all Species from current world
    /// </summary>
    /// <returns>A table of Scpecie object where :
    ///     - "name" is name of the specie
    ///     - "latinName" is latin name of the specie
    ///     - "classification" is agent's class number with engine formalism : family, genus and species
    ///     - "niche" is information about specie (how it feed, survive, etc)
    ///     - "caosFile" is the cos file where specie is programmed
    ///     - "quantity" count how many agent from this classicifcation are present in the current world
    ///     - "originPlace" is where are from specie (Norn Terrarium, Grendel Jungle, Ettin Desert or Aquarium)
    /// if request is OK. Return "FAILED" if KO.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     GET /World/Ecology
    /// </remarks>
    /// <response code="200">A table of Specie object</response>
    /// <response code="204">No Specie found in current world</response>
    /// <response code="500">Internal Error</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//World/[action]")]
    public ActionResult<List<Specie>> Ecology()
    {
        this._world.RefreshEcology();

        if (this._world.mySpeciesList.Count > 0)
        {
            return Ok(this._world.mySpeciesList.ToList());
        }
        else
        {
            return NoContent();
        }
    }

    /// <summary>
    /// Reset all Species from current world
    /// </summary>
    /// <returns>"SUCCESS" if request is OK. "FAILED" if at least one specie's réinject is KO.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /World/ResetEcology
    /// </remarks>
    /// <response code="200">World's Ecology have been reseted !</response>
    /// <response code="500">Internal Error</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//World/[action]")]
    public ActionResult<string> ResetEcology()
    {
        this._world.RefreshEcology();

        int verif = 0;

        foreach(var sp in this._world.mySpeciesList)
        {
            verif = verif + sp.ReInject();
        }

        if(verif == 0)
        {
            return Ok("SUCESS");
        }
        else
        {
            return StatusCode(500, "FAILED");
        }
    }

    /// <summary>
    /// Inject Agent from CAOS file from Bootstrap Folder
    /// </summary>
    /// <param name="Agent" example="hoverdoc">CAOS Filename without .cos extension</param>
    /// <returns>"OK" if command executed, "FAILED" if not.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /World/InjectAgentFromFileCaos?Agent=hoverdoc
    ///
    /// </remarks>
    /// <response code="200">Agent injected !</response>
    /// <response code="500">Internal Error</response>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//World/[action]")]
    public ActionResult<string> InjectAgentFromFileCaos([FromQuery, BindRequired] string Agent)
    {
        if (this._world.InjectCAOS(Agent) == 0)
        {
            return Ok("OK");
        }
        else
        {
            return StatusCode(500, "FAILED");
        }
    }

    /// <summary>
    /// Move all Eggs near Muco in Capillata's Norn Meso
    /// </summary>
    /// <returns>"OK" if success, "FAILED" if not</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     GET /World/MoveEggsMuco
    ///
    /// </remarks>
    /// <response code="200">Eggs have been moved</response>
    /// <response code="500">Internal Server Error</response>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [HttpPut]
    [Route("//World/[action]")]
    public ActionResult<string> EggsToMuco()
    {
        if (this._world.WorldUtil_MoveAllEggsNornNest_DS() == 0)
        {
            return Ok("OK");
        }
        else
        {
            return StatusCode(500, "FAILED");
        }
    }
}

