﻿using caos_WEB.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Net;
using System.Text.RegularExpressions;

namespace caos_WEB.Controllers;


// Resources documentaires :
// https://learn.microsoft.com/fr-fr/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-6.0&tabs=visual-studio
// https://learn.microsoft.com/fr-fr/aspnet/core/web-api/advanced/conventions?view=aspnetcore-6.0
// https://learn.microsoft.com/fr-fr/training/modules/improve-api-developer-experience-with-swagger/1-introduction


[ApiController]
[Produces("application/json")]
[Route("[controller]")]
public class CreatureController : ControllerBase
{
    private Creature _creature;
    private World _world;

    public CreatureController()
    {
        _creature = new Creature();
        _world = new World();
    }

    /// <summary>
    /// List all Creatures (Norn, Grendel, Ettin, Geat) in the current World
    /// </summary>
    /// <returns>Creatures with moniker, name, breed and gender. All other param are not fill for performance.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     GET /Creature/
    ///
    /// </remarks>
    /// <response code="200">Return Creatures (moniker, name, breed, gender)</response>
    /// <response code="204">No Creature found in current world</response>
    /// <response code="500">Internal Server Error</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creatures/")]
    [HttpGet]
    public ActionResult<List<Creature>> AllCreatures()
    {
        _world.RefreshCreaturesList();

        List<Creature> allCreatures = new List<Creature>();
        foreach (var crea in _world.myNornList.Concat(_world.myGrendelList).Concat(_world.myEttinList).Concat(_world.myGeatList))
        {
            allCreatures.Add(crea);
        }

        if(allCreatures.Count > 0)
        {
            return Ok(allCreatures.ToList());
        }
        else
        {
            return NoContent();
        }
        
    }

    /// <summary>
    /// Show full info of the Creature with the given Moniker
    /// </summary>
    /// <param name="moniker">Creature's Moniker</param>
    /// <returns>The Creatures with the given Moniker if exist</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     GET /Creature/072-twig-8tj7b-2te5v-6gunb-vvj6y
    ///
    /// </remarks>
    /// <response code="200">Returns the Creature with the specified Moniker</response>
    /// <response code="400">Not a valid Moniker</response>
    /// <response code="404">Creature not found with the specified Moniker</response>
    /// <response code="500">Internal Server Error</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creature/{moniker}")]
    public ActionResult<Creature> GetCreature(string moniker)
    {
        Regex rg = new Regex(@"([0-9]{1,})-[a-z]{4}(-[a-z0-9]{5}){4}$");

        if(rg.IsMatch(moniker))
        {
            var _creature = new Creature(moniker);
            if (_creature.MutationsNumber == -1)
            {
                return NotFound(moniker);
            }
            return Ok(_creature);
        }
        else
        {
            return BadRequest(moniker);
        }
        
    }

    /// <summary>
    /// Show the Creature's Life Event with the given Moniker
    /// </summary>
    /// <param name="moniker">Creature's Moniker</param>
    /// <returns>Creature's Life Event with the given Moniker if exist</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     GET /Creature/072-twig-8tj7b-2te5v-6gunb-vvj6y/LifeEvent
    ///
    /// </remarks>
    /// <response code="200">Returns the Creature's Life Event with the specified Moniker</response>
    /// <response code="400">Not a valid Moniker</response>
    /// <response code="404">Creature not found with the specified Moniker</response>
    /// <response code="500">Internal Server Error</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creature/{moniker}/[action]")]
    public ActionResult<List<LifeEvents>> LifeEvent(string moniker)
    {
        Regex rg = new Regex(@"([0-9]{1,})-[a-z]{4}(-[a-z0-9]{5}){4}$");

        if (rg.IsMatch(moniker))
        {
            var _creature = new Creature(moniker);
            _creature.GetHistory();
            return _creature.LifeEventsList.ToList();
        }
        else
        {
            return BadRequest(moniker);
        }
    }

    /// <summary>
    /// Show the Creature's Chemicals rate with the given Moniker
    /// </summary>
    /// <param name="moniker">Creature's Moniker</param>
    /// <returns>Creature's Chemicals rate with the given Moniker if exist</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     GET /Creature/072-twig-8tj7b-2te5v-6gunb-vvj6y/Chemical
    ///
    /// </remarks>
    /// <response code="200">Returns the Creature's Chemicals rate with the specified Moniker</response>
    /// <response code="400">Not a valid Moniker</response>
    /// <response code="404">Creature not found with the specified Moniker</response>
    /// <response code="500">Internal Server Error</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creature/{moniker}/[action]")]
    public ActionResult<List<ChemicalsProduct>> Chemical(string moniker)
    {
        Regex rg = new Regex(@"([0-9]{1,})-[a-z]{4}(-[a-z0-9]{5}){4}$");

        if (rg.IsMatch(moniker))
        {
            var _creature = new Creature(moniker);
            _creature.RefreshChemListInfos();
            return _creature.ChemList.ToList();
        }
        else
        {
            return BadRequest(moniker);
        }
    }

    /// <summary>
    /// Show the Creature's Organs rate with the given Moniker
    /// </summary>
    /// <param name="moniker">Creature's Moniker</param>
    /// <returns>Creature's Organs rate with the given Moniker if exist</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     GET /Creature/072-twig-8tj7b-2te5v-6gunb-vvj6y/Organ
    ///
    /// </remarks>
    /// <response code="200">Returns the Creature's Organ rate with the specified Moniker</response>
    /// <response code="400">Not a valid Moniker</response>
    /// <response code="404">Creature not found with the specified Moniker</response>
    /// <response code="500">Internal Server Error</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creature/{moniker}/[action]")]
    public ActionResult<List<CreatureOrgan>> Organ(string moniker)
    {
        Regex rg = new Regex(@"([0-9]{1,})-[a-z]{4}(-[a-z0-9]{5}){4}$");

        if (rg.IsMatch(moniker))
        {
            var _creature = new Creature(moniker);
            _creature.RefreshOrganInfos();
            return _creature.OrganList.ToList();
        }
        else
        {
            return BadRequest(moniker);
        }
    }

    /// <summary>
    /// Learn vocabulary to all Creatures of the world
    /// </summary>
    /// <returns>"OK" if command executed, "FAILED" if not.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /Creature/LearnVocabulary
    ///
    /// </remarks>
    /// <response code="200">All Creatures have been teached !</response>
    /// <response code="500">Internal Error</response>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creatures/[action]")]
    public ActionResult<string> LearnVocabulary()
    {
        var _world = new World();
        if(_world.WorldUtil_VocableAllCreatures() == 0)
        {
            return Ok("OK");
        }
        else
        {
            return StatusCode(500, "FAILED");
        }
    }

    /// <summary>
    /// Select and show in-game the Creature's given moniker.
    /// </summary>
    /// <returns>"OK" if command executed, "FAILED" if not.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /Creature/FocusCreature
    ///
    /// </remarks>
    /// <response code="200">Creature has been selected !</response>
    /// <response code="500">Internal Error</response>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creature/{moniker}/[action]")]
    public ActionResult<string> FocusCreature(string moniker)
    {
        var _creature = new Creature(moniker);

        if (_creature.CenterCamera() == 0 && _creature.SelectIngameCreature() == 0)
        {
            return Ok("OK");
        }
        else
        {
            return StatusCode(500, "FAILED");
        }
    }

    /// <summary>
    /// Age to Adult all Creatures of the world
    /// </summary>
    /// <returns>"OK" if command executed, "FAILED" if not.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /Creature/AgeAdultAll
    ///
    /// </remarks>
    /// <response code="200">All Creatures have been Aged to Adult !</response>
    /// <response code="500">Internal Error</response>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creatures/[action]")]
    public ActionResult<string> AgeAdultAll()
    {
        if (this._world.WorldUtil_AdultAllCreatures() == 0)
        {
            return Ok("OK");
        }
        else
        {
            return StatusCode(500, "FAILED");
        }
    }

    /// <summary>
    /// Kill Creatures with age >= threshold (in hours)
    /// </summary>
    /// <param name="Threshold" example="24">Creatures ages equal or than Threshold (in hours) are killed</param>
    /// <returns>"OK" if command executed, "FAILED" if not.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /Creature/KillImmortalCreature?Threshold=24
    ///
    /// </remarks>
    /// <response code="200">Immortal Creatures killed !</response>
    /// <response code="500">Internal Error</response>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Route("//Creatures/[action]")]
    public ActionResult<int> KillImmortalCreature([FromQuery, BindRequired] int Threshold)
    {
        int killCounter = -1;
        killCounter = this._world.WorldUtil_KillTooAgedCreature(Threshold);

        if (killCounter != -1)
        {
            return Ok(killCounter);
        }
        else
        {
            return StatusCode(500, "FAILED");
        }
    }
}
