using Microsoft.OpenApi.Models;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

// Add enum as string instead of int (ref: https://jasonwatmore.com/post/2021/10/12/net-return-enum-as-string-from-api )
builder.Services.AddControllers().AddJsonOptions(x =>
{
    // serialize enums as strings in api responses (e.g. Role)
    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());

    // Omit null fields in response (ref: https://stackoverflow.com/questions/44595027/net-core-remove-null-fields-from-api-json-response )
    x.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "caos-WEB API",
        Description = "Web API for managing Creatures 3 / Docking Station",
        //TermsOfService = new Uri("https://example.com/terms"),
        Contact = new OpenApiContact
        {
            Name = "Gitlab Project",
            Url = new Uri("https://gitlab.com/station96/creatures-enlightenment-ressources")
        },
        License = new OpenApiLicense
        {
            Name = "This project is licensed under the GNU General Public License v3.0",
            Url = new Uri("https://gitlab.com/station96/creatures-enlightenment-ressources/-/blob/ce22ab1b50e4749fa855b3ada33f434feb39edf7/LICENSE")
        }
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || app.Environment.IsProduction())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();