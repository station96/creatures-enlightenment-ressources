using System;
using System.Text.Json;
using System.Collections.Generic;

namespace CaosCLI02
{
    public class ChemicalsProduct
    {
        public int? Id { get; set; }
        public string? Name_EN { get; set; }
        public string? Name_FR { get; set; }
        public string? Description_EN { get; set; }
        public string? Description_FR { get; set; }

        public static Double ConversionTauxChem(string sVal, char Separator, long Precision)
        {
            string[] sDouble;
            Double Val;
        
            sDouble = sVal.Split(new char[1] { Separator });
            Val = Convert.ToInt64(sDouble[0]) * Precision + Convert.ToInt64(sDouble[1]);
            return Val / Precision;
        }
    }
}
