using System;
using CAOS;
using System.Text.Json;
using System.Collections.Generic;

namespace CaosCLI02
{
    public class Creature
    {
        public CaosInjector injectorDS = new CaosInjector("Docking Station");
        public DateTime LastRefreshDate { get; set; }
        public string? Moniker { get; private set; }
        public string? Name { get; set; }
        public string? Birthday { get; private set; }
        public TimeSpan Age;
        public LifeStage? CurrentLifeStage;
        public string? FirstParentMoniker { get; private set; }
        public string? SecondParentMoniker { get; private set; }
        public Breed Genus;
        public int? Generation { get; set; }
        public Sex Gender;
        public int? MutationsNumber { get; set; }
        public int? CrossOverPoint { get; set; }
        public int Tint_Red { get; set; } = 0;
        public int Tint_Green { get; set; } = 0;
        public int Tint_Blue { get; set; } = 0;
        public int Tint_Rotation { get; set; } = 0;
        public int Tint_Swap { get; set; } = 0;
        public string? WorldOfBirth { get; set; }
        public MonikerState? CurrentMonikerState;
        public int? OrganCount { get; set; } // Probablement temporaire pour l'instant le temps d'implémenter une Classe Organ
        private string _chemFileSource = "chemlist.json";
        private string _organFileSource = "organs.json";
        public List<ChemicalsProduct> ChemList = new List<ChemicalsProduct>();
        public List<double> ChemListValue = new List<double>();
        public List<CreatureOrgan> OrganList = new List<CreatureOrgan>();
        public List<double> OrganListValue = new List<double>();
        public List<LifeEvents> LifeEventsList = new List<LifeEvents>();

        public Creature(string moniker, string name, int genus, int sex)
        {
            this.Moniker = moniker;
            this.Name = name;
            
            switch (Convert.ToString(genus))
            {
                case "1":
                    Genus = Breed.Norn;
                    break;
                case "2":
                    Genus = Breed.Grendel;
                    break;
                case "3":
                    Genus = Breed.Ettin;
                    break;
                default:
                    Genus = Breed.Geat;
                    break;
            }

            switch (Convert.ToString(sex))
            {
                case "1":
                    this.Gender = Sex.Male;
                    break;
                case "2":
                    this.Gender = Sex.Female;
                    break;
                case "3":
                    this.Gender = Sex.Other;
                    break;
                case "-1":
                    this.Gender = Sex.Egg;
                    break;
                default:
                    this.Gender = Sex.Other;
                    break;
            }
        }
        public Creature(string moniker)
        {
            string req;
            CaosResult res;
            
            this.Moniker = moniker;

            // Get date and time for update LastRefresh (in fact, this is first)
            req = "outs rtif rtim \"%x %X\"";
            injectorDS.TryExecuteCaos(req, out res);
            LastRefreshDate = DateTime.Parse(res.Content.Trim('\0'));
            /*
            %a - Abbreviated weekday name
            %A - Full weekday name
            %b - Abbreviated month name
            %B - Full month name
            %c - Date and time representation appropriate for locale
            %d - Day of month as decimal number (01 - 31)
            %H - Hour in 24-hour format (00 - 23)
            %I - Hour in 12-hour format (01 - 12)
            %j - Day of year as decimal number (001 - 366)
            %m - Month as decimal number (01 - 12)
            %M - Minute as decimal number (00 - 59)
            %p - Current locale’s AM/PM indicator for 12-hour clock
            %S - Second as decimal number (00 - 59)
            %U - Week of year as decimal number, with Sunday as first day of week (00 - 53)
            %w - Weekday as decimal number (0 - 6; Sunday is 0)
            %W - Week of year as decimal number, with Monday as first day of week (00 - 53)
            %x - Date representation for current locale
            %X - Time representation for current locale
            %y - Year without century, as decimal number (00 - 99)
            %Y - Year with century, as decimal number
            %z, %Z - Time-zone name or abbreviation; no characters if time zone is unknown
            %% - Percent sign
            */

            this.RefreshNameInfo();
            this.RefreshGenusInfo();
            this.RefreshBirthdayInfo();
            this.RefreshGenderInfo();
            this.RefreshFirstParentMonikerInfo();
            this.RefreshSecondParentMonikerInfo();
            this.RefreshLifeStageInfo();
            this.RefreshAgeInfo();
            this.RefreshCrossOverPointsInfo();
            this.RefreshMutationsInfo();
            this.RefreshBirthWorldInfo();
            this.RefreshMonikerStateInfo();
            this.RefreshGenerationNumberInfo();
            this.RefreshOrganNumberInfo();
            this.RefreshTintInfo();

            /* [TODO]
                [X] Ajouter heure de récup des infos ?
                [X] Finir le Constructeur
                [X] Remplacer le Info1 par le Info2 dans le Main en utilisant la Classe
                [X] Implémenter une sélection de Creatures courante ?
                [X] Implémenter les Organes (concevoir une Classe Organ ?)
                [X] Implémenter les Chem (réuse de la Classe Chemicals ?) Faire pareil que les organes.
                [ ] Voir pour intégrer la valeur du chem dans la liste des chem et faire pareil pour les organes ? Car 2 listes défférentes en parallèle, c'est pas top !
                [X] Concevoir la Classe World pour taper les variables du jeu + recensement de population
                [ ] Modliser l'écosysteme avec recensement des bestioles hors Creatures dans classe World
                [X] Faire des fonctions "RefreshData()" et les appeler dans le Constructeur de classe ? surement mieux pour le re use de code
                [ ] Régler le problème d'Overflow par exception gérée sur l'age de la creature. Le TimeSpan ne doit pas être le bon type. Gérer plus simplement ?
                [ ] Voir pour retourner date/heure de Refresh sur chacune des fonctions Refresh ?
            */

            // Read ChemList from Json
            string jsonStringChemList = File.ReadAllText(_chemFileSource);
            ChemList = JsonSerializer.Deserialize<List<ChemicalsProduct>>(jsonStringChemList);

            // Read OrganList from Json
            string jsonStringOrganList = File.ReadAllText(_organFileSource);
            OrganList = JsonSerializer.Deserialize<List<CreatureOrgan>>(jsonStringOrganList);
        }

        public override string ToString ()
        {
            return $"breed: {this.Genus}; name: {this.Name}; gender: {this.Gender}; moniker: {this.Moniker}";
        }

        public void GetHistory()
        {
            LifeEventsList.Clear();
            
            string reqCountHist = $"outv hist coun \"{this.Moniker}\"";
            injectorDS.TryExecuteCaos(reqCountHist, out CaosResult resCountHist);

            int nbrLE = Convert.ToInt32(resCountHist.Content.Trim('\0'));
            Console.WriteLine($"Number of life events : {nbrLE}");

            CaosResult res;
            int[] CurrentCreatureLifeEvent = new int[nbrLE];
            TimeSpan[] CurrentCreatureTimeAgeEvent = new TimeSpan[nbrLE];
            DateTimeOffset[] CurrentCreatureDateOffsetEvent = new DateTimeOffset[nbrLE];
            string Associated_Moniker_1 = "";
            int Associated_Moniker_1_Type = 0;
            string Associated_Moniker_2 = "";
            int Associated_Moniker_2_Type = 0;

            for(int i = 0; i < nbrLE; i++)
            {
                injectorDS.TryExecuteCaos($"outv hist type \"{this.Moniker}\" {i}", out res);
                CurrentCreatureLifeEvent[i] = Convert.ToInt32(res.Content.Trim('\0'));
                
                injectorDS.TryExecuteCaos($"outv hist tage \"{this.Moniker}\" {i}", out res);
                CurrentCreatureTimeAgeEvent[i] = new TimeSpan(0, 0, Convert.ToInt32(res.Content.Trim('\0'))/20);

                injectorDS.TryExecuteCaos($"outv hist rtim \"{this.Moniker}\" {i}", out res);
                CurrentCreatureDateOffsetEvent[i] = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt32(res.Content.Trim('\0')));

                switch(CurrentCreatureLifeEvent[i])
                {
                    case 0: // Conceived , associated monikers are the mother's and father's
                    Associated_Moniker_1_Type = 1;
                    injectorDS.TryExecuteCaos($"outs hist mon1 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_1 = res.Content.Trim('\0');
                    
                    Associated_Moniker_2_Type = 2;
                    injectorDS.TryExecuteCaos($"outs hist mon2 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_2 = res.Content.Trim('\0');
                    break;
                    
                    case 1: // created using GENE CROS to crossover the two associated monikers
                    Associated_Moniker_1_Type = 3;
                    injectorDS.TryExecuteCaos($"outs hist mon1 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_1 = res.Content.Trim('\0');

                    Associated_Moniker_2_Type = 4;
                    injectorDS.TryExecuteCaos($"outs hist mon2 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_2 = res.Content.Trim('\0');
                    break;
                    
                    case 2: // Engineered - from a human made genome with GENE LOAD, the first associated moniker is blank, and the second is the filename
                    Associated_Moniker_1_Type = 0;
                    Associated_Moniker_1 = string.Empty;

                    Associated_Moniker_2_Type = 6;
                    injectorDS.TryExecuteCaos($"outs hist mon2 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_2 = res.Content.Trim('\0');
                    break;

                    case 14: // Cloned associated moniker is who we were cloned from
                    Associated_Moniker_1_Type = 0;
                    Associated_Moniker_1 = string.Empty;

                    Associated_Moniker_2_Type = 7;
                    injectorDS.TryExecuteCaos($"outs hist mon2 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_2 = res.Content.Trim('\0');
                    break;

                    case 3: // Born, associated monikers are the parents
                    Associated_Moniker_1_Type = 1;
                    injectorDS.TryExecuteCaos($"outs hist mon1 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_1 = res.Content.Trim('\0');

                    Associated_Moniker_2_Type = 2;
                    injectorDS.TryExecuteCaos($"outs hist mon2 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_2 = res.Content.Trim('\0');
                    break;

                    case 8: // Became pregnant - the first associated moniker is the child, and the second the father
                    Associated_Moniker_1_Type = 5;
                    injectorDS.TryExecuteCaos($"outs hist mon1 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_1 = res.Content.Trim('\0');

                    Associated_Moniker_2_Type = 2;
                    injectorDS.TryExecuteCaos($"outs hist mon2 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_2 = res.Content.Trim('\0');
                    break;

                    case 9: // Impregnated - first associated moniker is the child, second the mother
                    Associated_Moniker_1_Type = 5;
                    injectorDS.TryExecuteCaos($"outs hist mon1 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_1 = res.Content.Trim('\0');

                    Associated_Moniker_2_Type = 1;
                    injectorDS.TryExecuteCaos($"outs hist mon2 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_2 = res.Content.Trim('\0');
                    break;

                    case 10: // Child born - first moniker is the child, second the other parent
                    Associated_Moniker_1_Type = 5;
                    injectorDS.TryExecuteCaos($"outs hist mon1 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_1 = res.Content.Trim('\0');

                    if(this.Gender == (Sex)1)
                    {
                        Associated_Moniker_2_Type = 1;
                    }
                    else
                    {
                        Associated_Moniker_2_Type = 2;
                    }
                    injectorDS.TryExecuteCaos($"outs hist mon2 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_2 = res.Content.Trim('\0');
                    break;

                    case 15: // Clone source - someone was cloned from you, first moniker is whom
                    Associated_Moniker_1_Type = 8;
                    injectorDS.TryExecuteCaos($"outs hist mon1 \"{this.Moniker}\" {i}", out res);
                    Associated_Moniker_1 = res.Content.Trim('\0');

                    Associated_Moniker_2_Type = 0;
                    Associated_Moniker_2 = string.Empty;
                    break;

                    default:
                    Associated_Moniker_1_Type = 0;
                    Associated_Moniker_2_Type = 0;
                    Associated_Moniker_1 = string.Empty;
                    Associated_Moniker_2 = string.Empty;
                    break;
                }

                LifeEventsList.Add(new LifeEvents(CurrentCreatureLifeEvent[i], CurrentCreatureTimeAgeEvent[i], CurrentCreatureDateOffsetEvent[i].LocalDateTime,Associated_Moniker_1_Type,Associated_Moniker_1, Associated_Moniker_2_Type, Associated_Moniker_2));
            }
        }

        public int CenterCamera ()
        {
            // Center Camera on Creature's body
            string req = $"targ mtoa \"{this.Moniker}\" trck null 0 0 0 0 doif hhld <> targ doif room targ <> -1 and carr <> game \"c3_inventory\" cmrt 0 endi endi trck norn 70 70 1 0";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            if(res.Success)
            {
                return 0; // Success
            }
            else
            {
                Console.WriteLine($"Error : {res.Content}");
                return 1; // Fail
            }
        }
        public int SelectIngameCreature ()
        {
            // Select Creature ingame
            string req = $"norn mtoa \"{this.Moniker}\"";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            if(res.Success)
            {
                return 0; // Success
            }
            else
            {
                Console.WriteLine($"Error : {res.Content}");
                return 1; // Fail
            }
        }
        
        public void RefreshNameInfo()
        {
            // Get Name
            //string req = $"outs hist name \"{Moniker}\"";
            injectorDS.TryExecuteCaos($"outs hist name \"{Moniker}\"", out CaosResult res);
            this.Name = res.Content.Trim('\0');
        }

        public void RefreshGenusInfo()
        {
            // Get Genus
            string req = $"outv hist gnus \"{Moniker}\"";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            string resultat = res.Content.Trim('\0');
            switch (resultat)
            {
                case "1":
                    Genus = Breed.Norn;
                    break;
                case "2":
                    Genus = Breed.Grendel;
                    break;
                case "3":
                    Genus = Breed.Ettin;
                    break;
                default:
                    Genus = Breed.Geat;
                    break;
            }
        }

        public void RefreshBirthdayInfo()
        {
            // Get Birthday
            string req = $"outs rtif hist rtim \"{Moniker}\" 3 \"%x %X\"";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            if(res.Content.Contains("Life event doesn't existe for this moniker"))
            {
                injectorDS.TryExecuteCaos($"outs rtif hist rtim \"{Moniker}\" 1 \"%x %X\"", out CaosResult resSpliced);
                this.Birthday = resSpliced.Content.Trim('\0');
            }
            else
            {
                this.Birthday = res.Content.Trim('\0');
            }            
        }

        public void RefreshGenderInfo()
        {
            // Get Gender
            string req = $"outv hist gend \"{Moniker}\"";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            string resultat = res.Content.Trim('\0');
            switch (resultat)
            {
                case "1":
                    Gender = Sex.Male;
                    break;
                case "2":
                    Gender = Sex.Female;
                    break;
                case "-1":
                    Gender = Sex.Egg;
                    break;
                default:
                    Gender = Sex.Other;
                    break;
            }
        }

        public void RefreshFirstParentMonikerInfo()
        {
            // Get First Parent moniker
            string req = $"outs hist mon1 \"{Moniker}\" 0";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            this.FirstParentMoniker = res.Content.Trim('\0');
        }

        public void RefreshSecondParentMonikerInfo()
        {
            // Get Second Parent moniker
            string req = $"outs hist mon2 \"{Moniker}\" 0";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            this.SecondParentMoniker = res.Content.Trim('\0');
        }

        public void RefreshLifeStageInfo()
        {
            // Get Lifestage
            string req = $"targ mtoa \"{Moniker}\" outv cage";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            if (Int32.TryParse(res.Content.Trim('\0'), out int outLifeStage))
            {
                this.CurrentLifeStage = (LifeStage)outLifeStage;
            }
            else
            {
                this.CurrentLifeStage = null;
            }
        }

        public void RefreshAgeInfo()
        {
            // Get Age
            string req = $"targ mtoa \"{Moniker}\" setv va01 tage divv va01 20 setv va03 va01 modv va03 60 divv va01 60 setv va02 va01 modv va02 60 divv va01 60 outv va01 outs \":\" outv va02 outs \":\" outv va03";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            try {
                this.Age = TimeSpan.Parse(res.Content.Trim('\0'));
            }
            catch (FormatException) {
               Console.WriteLine("{0}: Bad Format", res.Content.Trim('\0'));
            }   
            catch (OverflowException) {
               Console.WriteLine("{0}: Overflow", res.Content.Trim('\0'));
            }
        }

        public void RefreshCrossOverPointsInfo()
        {
            // Get Crossover Points
            string req = $"outv hist cros \"{Moniker}\""; // REQ OK
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            if (Int32.TryParse(res.Content.Trim('\0'), out int tempCOP))
            {
                this.CrossOverPoint = tempCOP;
            }
            else
            {
                this.CrossOverPoint = null;
            }
        }

        public void RefreshMutationsInfo()
        {
            // Get Mutations
            string req = $"outv hist mute \"{Moniker}\""; // REQ OK
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            if (Int32.TryParse(res.Content.Trim('\0'), out int tempMUT))
            {
                this.MutationsNumber = tempMUT;
            }
            else
            {
                this.MutationsNumber = null;
            }
        }

        public void RefreshTintInfo()
        {
            // Get Tint Infos
            CaosResult resTintRed, resTintGreen, resTintBlue, resTintRotation, resTintSwap;
            
            injectorDS.TryExecuteCaos($"targ mtoa \"{Moniker}\" outv tint 1", out resTintRed);
            injectorDS.TryExecuteCaos($"targ mtoa \"{Moniker}\" outv tint 2", out resTintGreen);
            injectorDS.TryExecuteCaos($"targ mtoa \"{Moniker}\" outv tint 3", out resTintBlue);
            injectorDS.TryExecuteCaos($"targ mtoa \"{Moniker}\" outv tint 4", out resTintRotation);
            injectorDS.TryExecuteCaos($"targ mtoa \"{Moniker}\" outv tint 5", out resTintSwap);
            
            if (Int32.TryParse(resTintRed.Content.Trim('\0'), out int _TintRed))
            {
                this.Tint_Red = _TintRed;
            }
            else
            {
                this.Tint_Red = 0;
            }
            if (Int32.TryParse(resTintGreen.Content.Trim('\0'), out int _TintGreen))
            {
                this.Tint_Green = _TintGreen;
            }
            else
            {
                this.Tint_Red = 0;
            }
            if (Int32.TryParse(resTintBlue.Content.Trim('\0'), out int _TintBlue))
            {
                this.Tint_Blue = _TintBlue;
            }
            else
            {
                this.Tint_Red = 0;
            }
            if (Int32.TryParse(resTintRotation.Content.Trim('\0'), out int _TintRotation))
            {
                this.Tint_Rotation = _TintRotation;
            }
            else
            {
                this.Tint_Red = 0;
            }
            if (Int32.TryParse(resTintSwap.Content.Trim('\0'), out int _TintSwap))
            {
                this.Tint_Swap = _TintSwap;
            }
            else
            {
                this.Tint_Red = 0;
            }
        }

        public void RefreshBirthWorldInfo()
        {
            // Get the BirthWorld
            string req = $"outs hist wnam \"{Moniker}\" 0";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            this.WorldOfBirth = res.Content.Trim('\0');
        }

        public void RefreshMonikerStateInfo()
        {
            // Get Moniker's Status
            string req = $"outv ooww \"{Moniker}\"";
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            if (Int32.TryParse(res.Content.Trim('\0'), out int outState))
            {
                this.CurrentMonikerState = (MonikerState)outState;
            }
            else
            {
                this.CurrentMonikerState = null;
            }
        }

        public void RefreshGenerationNumberInfo()
        {
            // Returns Generation by Spliting Moniker
            if(this.Moniker != null)
            {
                string[] ExplodedMoniker = this.Moniker.Split('-');
                if (Int32.TryParse(ExplodedMoniker[0], out int outGen))
                {
                    this.Generation = outGen;
                }
                else
                {
                    this.Generation = null;
                }
            }
            else
            {
                this.Generation = null;
            }
        }

        public void RefreshOrganNumberInfo()
        {
            // Returns the number of organs in target creature
            string req = $"targ mtoa \"{Moniker}\" outv orgn"; // A TESTER
            injectorDS.TryExecuteCaos(req, out CaosResult res);
            if (Int32.TryParse(res.Content.Trim('\0'), out int outOrgn))
            {
                this.OrganCount = outOrgn;
            }
            else
            {
                this.OrganCount = null;
            }
        }

        public void RefreshOrganInfos()
        {
            int currentOrgan = 0;
            foreach (var organ in this.OrganList)
            {
                if(organ.Id == 12 && this.Gender == Sex.Male) // If male, no uterus ;-) so skip
                {
                    this.OrganListValue.Add(0);
                    continue;
                }
                injectorDS.TryExecuteCaos($"targ mtoa \"{Moniker}\" outv orgf {currentOrgan} 1", out CaosResult result);
                Double taux = Math.Round(CreatureOrgan.ConversionTauxOrgan(result.Content, '.', 100) / 100, 1);
                
                this.OrganListValue.Add(taux);
                currentOrgan ++;
            }
        }

        public void GetOrganList()
        {
            int i = 0;
            foreach(var organ in this.OrganList)
            {
                Console.WriteLine($"{organ.Organ} : {this.OrganListValue[i]} %");
                i++;
            }
        }

        public void RefreshChemListInfos()
        {
            int currentChem = 0;
            foreach(var chem in this.ChemList)
            {
                injectorDS.TryExecuteCaos($"targ norn outv chem {currentChem}", out CaosResult result);
                Double taux = Math.Round(ChemicalsProduct.ConversionTauxChem(result.Content.Trim('\0'), '.', 100) / 100, 2);
                //if(taux == 0) continue;
                //Console.WriteLine($"{this.ChemList[currentChem].Id} - {this.ChemList[currentChem].Name_EN} : {taux}%");
                this.ChemListValue.Add(taux);
                currentChem++;
            }
        }

        public void GetChemList()
        {
            int i = 0;
            foreach(var chem in this.ChemList)
            {
                if(chem.Name_EN == "Unknownase")
                {
                    continue;
                }
                Console.WriteLine($"{chem.Name_EN} : {this.ChemListValue[i]} %");
                i++;
            }
        }

        /*
        la lecture du json se fait où ? dans le constructeur de la créature ? Surprenant, on le fait qu'une fois pour toute normalement !
        même question pour le organe liste
        Probablement un override du writeline pour afficher la liste et intégranet la boucle qu'il y a dans le main
        il y a un truc avec la liste des chem avec une surcharge :
            - pas d'argument -> affichage complet
            - argument -> affichage d'un seul produit
        
        public void Refresh ? ChemList (moniker, [1 produit optionnel])
        pour le moniker envoyé
        de 1 à 255
        lit les produits chimiques
        fin

        */
    }

    public enum Sex { Egg = -1, Male = 1, Female = 2, Other = 3 }
        public enum Breed { Norn = 1, Grendel = 2, Ettin = 3, Geat = 4 }
        public enum LifeStage {
            Baby = 0,
            Child = 1,
            Adolescent = 2,
            Youth = 3,
            Adult = 4,
            Old = 5,
            Ancient = 6,
            Death = 7
        }
        public enum MonikerState
        {
            NeverExistedOrPurged = 0, // never existed, or history purged
            SlotReferenceEgg = 1, // genome referenced by a slot, for example an egg
            MadeWithNewCrea = 2, // creature made with NEW: CREA
            ProperlyBorn = 3, // creature properly BORN
            Exported = 4, // out of world, exported
            DeadWithBody = 5, // dead, body still exists
            DeadWithoutBody = 6, // dead, body KILLed
            Unreferenced = 7 // unreferenced genome
        }

}