using System;
using System.Text.Json;
using System.Collections.Generic;

namespace CaosCLI02
{
    public class CreatureOrgan
    {
        public int? Id { get; set; }
        public string? Organ { get; set; }
        public string? Description { get; set; }

        public static Double ConversionTauxOrgan(string sVal, char Separator, long Precision)
        {
            string[] sDouble;
            Double Val;
        
            sDouble = sVal.Split(new char[1] { Separator });
            Val = Convert.ToInt64(sDouble[0]) * Precision + Convert.ToInt64(sDouble[1]);
            return Val / Precision;
        }
    }

    /*public class CreatureOrganList
    {
        public List<CreatureOrgan>? OrganList = new List<CreatureOrgan>();
        
        public CreatureOrganList()
        {
            string fileName = "organs.json";
            string jsonString = File.ReadAllText(fileName);

            OrganList = JsonSerializer.Deserialize<List<CreatureOrgan>>(jsonString);
        }

    }*/
}
