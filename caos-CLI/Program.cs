﻿using CAOS;

namespace CaosCLI02
{
    static class ClassIndex
    {
        public static int InputIndex = 0;
        public static Creature? myCreature;
        
        public static World myWorld = new World();
    }

    class App
    {
        private class MenuItem
        {
            public string? Description { get; set; }
            public Action? Execute { get; set; }
        }

        private static List<MenuItem>? MenuItems;

        private static void PopulateMenuItems()
        {
            MenuItems = new List<MenuItem>
            {
                new MenuItem {Description = "Show Creatures List", Execute = ShowCreaturesList},
                new MenuItem {Description = "Current Creature's Information", Execute = CreatureInfo},
                new MenuItem {Description = "Show (and select) current Creatures in-game", Execute = FocusCurrentCreature},
                new MenuItem {Description = "Current Creature's Life events", Execute = LifeEvents},
                new MenuItem {Description = "Current Creature's Organs Status", Execute = CreatureOrgansInfo},
                new MenuItem {Description = "Current Creature's Blood Status", Execute = CreatureChemsInfo},
                new MenuItem {Description = "Move all eggs to Norn Nest (DS)", Execute = MoveAllEggsNornNest_DS},
                new MenuItem {Description = "Learn vocable all Creatures", Execute = VocabAll},
                new MenuItem {Description = "Ages Creatures to Adult", Execute = AgesAdultAll},
                new MenuItem {Description = "Kill bacterias", Execute = BacteriaDestruction},
                new MenuItem {Description = "Ecology Status", Execute = EcologyInfo},
                new MenuItem {Description = "Ecology Reset", Execute = RebuildEcology},
                new MenuItem {Description = "Inject CAOS file (from Bootstrap folder and sub-folder)", Execute = InjectAgentFromFileCaos},
                new MenuItem {Description = "List all Game Variables", Execute = VarList},
                new MenuItem {Description = "Quit", Execute = Quit},
            };
        }

        private static void EcologyInfo()
        {
            Console.Clear();
            Console.WriteLine("-- World 's Ecology Status --");
            Console.WriteLine(Environment.NewLine);

            ClassIndex.myWorld.RefreshEcology();

            foreach(var sp in ClassIndex.myWorld.mySpeciesList)
            {
                Console.WriteLine($"{sp.Name} : {sp.Quantity}");
            }

            GoToMainMenu();
        }

        private static void RebuildEcology()
        {
            Console.Clear();
            Console.WriteLine("-- Rebuilt Ecology --");
            Console.WriteLine(Environment.NewLine);

            ClassIndex.myWorld.RefreshEcology();

            int i = 0;
            foreach(var sp in ClassIndex.myWorld.mySpeciesList)
            {
                sp.ReInject();
                i++;
            }

            Console.WriteLine($"Ecology of this World was reset !\n{i} species reintroduced...");

            GoToMainMenu();
        }

        private static void MoveAllEggsNornNest_DS()
        {
            Console.Clear();
            if(ClassIndex.myWorld.WorldUtil_MoveAllEggsNornNest_DS() == 0)
            {
                Console.WriteLine("Result : OK");
            }
            else
            {
                Console.WriteLine("Result : FAILED");
            }

            GoToMainMenu();
        }

        private static void LifeEvents()
        {
            Console.Clear();
            Console.WriteLine("-- Life Eventes of selected Creatures --");
            if(ClassIndex.myCreature != null)
            {
                Console.WriteLine(ClassIndex.myCreature.ToString());
            }
            Console.WriteLine(Environment.NewLine);

            try
            {
                ClassIndex.myCreature.GetHistory();

                int indexEvent = 0;
                foreach(var le in ClassIndex.myCreature.LifeEventsList)
                {
                    string _evenements =
$@"
    Event number: {indexEvent};
    Event Type: {le.LifeEventRef};
    Creature Age: {le.CreatureAge};
    Event Date: {le.EventDate.ToString()};
    First Associated Moniker Type: {le.Associated_Moniker_1_Type.ToString()};
    First Associated Moniker: {le.Associated_Moniker_1};
    Second Associated Moniker Type: {le.Associated_Moniker_2_Type.ToString()};
    Second Associated Moniker: {le.Associated_Moniker_2}
";

                    Console.WriteLine(_evenements);
                    indexEvent++;
                }
            }
            catch (NullReferenceException) {
               Console.WriteLine("Select Creature in [1. Show Creatures List] please...");
            }

            GoToMainMenu();
        }

        private static void ShowCreaturesList()
        {
            Console.Clear();
            Console.WriteLine("-- List All Creatures --");
            if(ClassIndex.myCreature != null)
            {
                Console.WriteLine(ClassIndex.myCreature.ToString());
            }
            Console.WriteLine(Environment.NewLine);

            ClassIndex.myWorld.RefreshCreaturesList();

            List<Creature> ListeComplete = new List<Creature>();
            int index = 0;
            foreach(var crea in ClassIndex.myWorld.myNornList.Concat(ClassIndex.myWorld.myGrendelList).Concat(ClassIndex.myWorld.myEttinList).Concat(ClassIndex.myWorld.myGeatList))
            {
                ListeComplete.Add(crea);
                Console.WriteLine($"{index} - {crea.ToString()}");
                index++;
            }

            Console.WriteLine(Environment.NewLine);
            Console.WriteLine($"Norn : {ClassIndex.myWorld.NornPopulation} ({ClassIndex.myWorld.NornPopulationMale} male - {ClassIndex.myWorld.NornPopulationFemale} Female) and {ClassIndex.myWorld.NornPopulationEgg} eggs.");
            Console.WriteLine($"Ettin : {ClassIndex.myWorld.EttinPopulation} ({ClassIndex.myWorld.EttinPopulationMale} male - {ClassIndex.myWorld.EttinPopulationFemale} Female) and {ClassIndex.myWorld.EttinPopulationEgg} eggs.");
            Console.WriteLine($"Grendel : {ClassIndex.myWorld.GrendelPopulation} ({ClassIndex.myWorld.GrendelPopulationMale} male - {ClassIndex.myWorld.GrendelPopulationFemale} Female) and {ClassIndex.myWorld.GrendelPopulationEgg} eggs.");
            Console.WriteLine($"Geat : {ClassIndex.myWorld.GeatPopulation} ({ClassIndex.myWorld.GeatPopulationMale} male - {ClassIndex.myWorld.GeatPopulationFemale} Female) and {ClassIndex.myWorld.GeatPopulationEgg} eggs.");
            Console.WriteLine(Environment.NewLine);

            // Get the user input
            int choice = 0;
            do
            {
                Console.Write($"Select Creature (0 - {ListeComplete.Count-1}) : ");
            } while (!int.TryParse(Console.ReadLine(), out choice) ||
                    choice < 0 || choice > ListeComplete.Count-1);

            ClassIndex.myCreature = new Creature(ListeComplete[choice].Moniker);

            Console.WriteLine($"Selected Creature : {ClassIndex.myCreature.ToString()} ");
            
            GoToMainMenu();
        }
        
        private static void VarList()
        {
            Console.Clear();
            Console.WriteLine("-- Game variables info --");
            Console.WriteLine(Environment.NewLine);

            ClassIndex.myWorld.GetGameVariable();
            
            GoToMainMenu();
        }

        private static void FocusCurrentCreature()
        {
            Console.Clear();

            try
            {
                Console.WriteLine(ClassIndex.myCreature.ToString());
                if(ClassIndex.myCreature.CenterCamera() == 0 && ClassIndex.myCreature.SelectIngameCreature() == 0)
                {
                    Console.WriteLine("Result : OK");
                }
                else
                {
                    Console.WriteLine("Result : FAILED");
                }
            }
            catch (NullReferenceException) {
               Console.WriteLine("Select Creature in [1. Show Creatures List] please...");
            }     

            GoToMainMenu();
        }
        
        private static void CreatureOrgansInfo()
        {
            Console.Clear();
            Console.WriteLine("-- Organs Status --");
            if(ClassIndex.myCreature != null)
            {
                Console.WriteLine(ClassIndex.myCreature.ToString());
            }
            Console.WriteLine(Environment.NewLine);
            
            try
            {
                ClassIndex.myCreature.RefreshOrganInfos();
                ClassIndex.myCreature.GetOrganList();
            }
            catch (NullReferenceException) {
                Console.WriteLine("Select Creature in [1. Show Creatures List] please...");
            }
            
            GoToMainMenu();
        }

        private static void CreatureChemsInfo()
        {
            Console.Clear();
            Console.WriteLine("-- Blood Status --");
            if(ClassIndex.myCreature != null)
            {
                Console.WriteLine(ClassIndex.myCreature.ToString());
            }

            try
            {
                Console.WriteLine(Environment.NewLine);
                ClassIndex.myCreature.RefreshChemListInfos();
                ClassIndex.myCreature.GetChemList();
            }
            catch (NullReferenceException) {
               Console.WriteLine("Select Creature in [1. Show Creatures List] please...");
            }
            
            GoToMainMenu();
        }
        
        private static void CreatureInfo()
        {
            Console.Clear();
            Console.WriteLine("-- Informations Creatures sélectionnée --");
            if(ClassIndex.myCreature != null)
            {
                Console.WriteLine(ClassIndex.myCreature.ToString());
            }
            Console.WriteLine(Environment.NewLine);
            
            try
            {
                Console.WriteLine($"Last Refresh Date & Time : {ClassIndex.myCreature.LastRefreshDate}");
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine($"Moniker : {ClassIndex.myCreature.Moniker}");
                Console.WriteLine($"Name : {ClassIndex.myCreature.Name}");
                Console.WriteLine($"Birthday : {ClassIndex.myCreature.Birthday}");
                Console.WriteLine($"Age : {ClassIndex.myCreature.Age}");
                Console.WriteLine($"LifeStage : {ClassIndex.myCreature.CurrentLifeStage}");
                Console.WriteLine($"Gender : {ClassIndex.myCreature.Gender}");
                Console.WriteLine($"Breed : {ClassIndex.myCreature.Genus}");
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine($"Generation : {ClassIndex.myCreature.Generation}");
                Console.WriteLine($"Crossover Point : {ClassIndex.myCreature.CrossOverPoint}");
                Console.WriteLine($"Mutations : {ClassIndex.myCreature.MutationsNumber}");
                Console.WriteLine($"Pigment Genes Red: {ClassIndex.myCreature.Tint_Red}");
                Console.WriteLine($"Pigment Genes Green : {ClassIndex.myCreature.Tint_Green}");
                Console.WriteLine($"Pigment Genes Blue : {ClassIndex.myCreature.Tint_Blue}");
                Console.WriteLine($"Pigment Rotation (Bleed Genes) : {ClassIndex.myCreature.Tint_Rotation}");
                Console.WriteLine($"Pigment Swap (Bleed Genes) : {ClassIndex.myCreature.Tint_Swap}");
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine($"Organ Number : {ClassIndex.myCreature.OrganCount}");
                Console.WriteLine($"Birth's World : {ClassIndex.myCreature.WorldOfBirth}");
                Console.WriteLine($"Creature's Status : {ClassIndex.myCreature.CurrentMonikerState}");
                Console.WriteLine($"First Parent Moniker : {ClassIndex.myCreature.FirstParentMoniker}");
                Console.WriteLine($"Second Parent Moniker : {ClassIndex.myCreature.SecondParentMoniker}");
            }
            catch (NullReferenceException) {
               Console.WriteLine("Select Creature in [1. Show Creatures List] please...");
            }            

            GoToMainMenu();
        }      
        private static void InjectAgentFromFileCaos()
        {
            Console.Clear();
            Console.WriteLine("-- Inject from CAOS file (Bootstrap folder) --");
            Console.WriteLine("Name of the file (without .cos extension)");
            
            if(ClassIndex.myWorld.InjectCAOS(Console.ReadLine()) == 0)
            {
                Console.WriteLine($"File inject : OK");
            }
            else
            {
                Console.WriteLine($"File inject : FAILED");
            }

            GoToMainMenu();
        }
        
        private static void VocabAll()
        {
            Console.Clear();
            if(ClassIndex.myWorld.WorldUtil_VocableAllCreatures() == 0)
            {
                Console.WriteLine("Result : OK");
            }
            else
            {
                Console.WriteLine("Result : FAILED");
            }

            GoToMainMenu();
        }

        private static void AgesAdultAll()
        {
            Console.Clear();
            if(ClassIndex.myWorld.WorldUtil_AdultAllCreatures() == 0)
            {
                Console.WriteLine("Result : OK");
            }
            else
            {
                Console.WriteLine("Result : FAILED");
            }

            GoToMainMenu();
        }
        private static void BacteriaDestruction()
        {
            Console.Clear();
            if(ClassIndex.myWorld.WorldUtil_BacteriaDestruction() == 0)
            {
                Console.WriteLine("Result : OK");
            }
            else
            {
                Console.WriteLine("Result : FAILED");
            }

            GoToMainMenu();
        }

        private static void Quit()
        {
            Console.Clear();
            Console.WriteLine("Thanks and have a nice day...");
            Environment.Exit(0);
        }

        private static void ClearAndShowHeading(string heading)
        {
            Console.Clear();
            Console.WriteLine(heading);
            Console.WriteLine(new string('-', heading?.Length ?? 0));
        }

        private static void GoToMainMenu()
        {
            Console.Write("\nPress any key to continue...");
            Console.ReadKey();
            MainMenu();
        }

        static void MainMenu()
        {
            ClearAndShowHeading("---------- Welcome to Caos-CLI [version 0.2] ---------");
            if(ClassIndex.myCreature != null)
            {
                Console.WriteLine(ClassIndex.myCreature.ToString());
            }
            Console.WriteLine(Environment.NewLine);
            
             // Write out the menu options
            for (int i = 0; i < MenuItems.Count; i++)
            {
                Console.WriteLine($"  {i + 1}. {MenuItems[i].Description}");
            }

            // Get the cursor position for later use 
            // (to clear the line if they enter invalid input)
            int cursorTop = Console.CursorTop + 1;
            int userInput;

            // Get the user input
            do
            {
                // These three lines clear the previous input so we can re-display the prompt
                Console.SetCursorPosition(0, cursorTop);
                Console.Write(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(0, cursorTop);

                Console.Write($"Choice (1 - {MenuItems.Count}) : ");
            } while (!int.TryParse(Console.ReadLine(), out userInput) ||
                    userInput < 1 || userInput > MenuItems.Count);

            // Execute the menu item function
            ClassIndex.InputIndex = userInput - 1;
            MenuItems[userInput - 1].Execute();
        }

        static void Main(string[] args)
        {
            PopulateMenuItems();
            MainMenu();

            Console.Write("\nPress any key to quit...");
            Console.ReadKey();
        }
    }
}