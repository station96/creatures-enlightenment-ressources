# caos-CLI

A CLI tools to connect Docking Station and view informations about World, Creatures, Agents, etc...

**Please be sure DS is lauched before executing.**

![Welcome Screen](/assets/images/caos-CLI-001.png)

Please be kind : this is my first projects in C# : code is ugly but i'm learning ;-)

## Built with

- Love ;-)
- **CAOS.dll** from [AlbianWarp/Caos-Class-Library](https://github.com/AlbianWarp/Caos-Class-Library).
- Visual Studio Code and .Net6 Framework
- Windows 10

## Requirements

- .NET 6.0 Runtime [here](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime)
- Docking Station of course : available for free [here](https://store.steampowered.com/app/1659050/Creatures_Docking_Station/)

## Installation

- Clone repo or download at least the directory ```\caos-CLI\bin\Debug\net6.0```
- Unzip anywhere on you computer
- Run Docking Station and your world
- Run ```caos-cli-v2.exe```

## Usage

- Select a Creature with first option for exemple and test other ;-)

You can :

- List all Creatures (Norn, Grendel, Ettin, Geat) in the current World
- Show full info of the Creature with the given Moniker
- Show the Creature's Life Event with the given Moniker
- Show the Creature's Chemicals rate with the given Moniker
- Show the Creature's Organs rate with the given Moniker
- Learn vocabulary to all Creatures of the world
- Select and show in-game the Creature's given moniker
- Age to Adult all Creatures of the world
- Move all Eggs near Muco in Capillata's Norn Meso
- List all Species from current world
- Reset all Species from current world
- Inject Agent from CAOS file from Bootstrap Folder
- List all GAME variable

![Creatures List](/assets/images/caos-CLI-002.png)

![Creature Infos](/assets/images/caos-CLI-003.png)

![Creature Life Events List](/assets/images/caos-CLI-004.png)

![World Ecology](/assets/images/caos-CLI-005.png)

## Contributing

Any contributions you could make are greatly appreciated.

## License

Distributed under the GNU General Public License v3.0. See ``LICENSE`` in main directory for more information.
