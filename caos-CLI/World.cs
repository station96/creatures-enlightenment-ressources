using System;
using CAOS;
using System.Collections.Generic;

namespace CaosCLI02
{
    public class World
    {
        public CaosInjector injector = new CaosInjector("Docking Station");
        // --------- START GAME VARIABLE ------------ //
        // --- reference : https://de.wikibooks.org/wiki/CAOS-Programmierung:_Befehlsgruppen:_Game_Variables --- ///
        public int Bramboo_LocalSphere
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Bramboo_LocalSphere\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Bramboo_MaxPop_Local
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Bramboo_MaxPop_Local\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Carrot_LocalSphere
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Carrot_LocalSphere\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Carrot_MaxPop_Local
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Carrot_MaxPop_Local\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Grettin
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Grettin\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Stinger_MaxPop_Global
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Stinger_MaxPop_Global\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int ToolTipDev
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"ToolTipDev\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Trapper_LocalSphere
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Trapper_LocalSphere\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Trapper_MaxPop_Global
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Trapper_MaxPop_Global\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Trapper_MaxPop_Local
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Trapper_MaxPop_Local\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int Trapper_MinPop_Global
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"Trapper_MinPop_Global\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int breeding_limit
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"breeding_limit\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_after_shee_dates
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_after_shee_dates\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_creature_accg
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_creature_accg\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_creature_attr
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_creature_attr\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_creature_bhvr
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_creature_bhvr\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_creature_perm
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_creature_perm\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_default_focus_part
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_default_focus_part\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_magic_delay
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_magic_delay\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_max_creatures
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_max_creatures\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_max_norns
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_max_norns\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int c3_meta_transition
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"c3_meta_transition\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int ds_game_type
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"ds_game_type\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int ds_number_of_life_events
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"ds_number_of_life_events\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int ds_number_of_warnings
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"ds_number_of_warnings\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_LengthOfDayInMinutes
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_LengthOfDayInMinutes\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_LengthOfSeasonInDays
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_LengthOfSeasonInDays\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_NumberOfSeasons
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_NumberOfSeasons\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_SkeletonUpdateDoubleSpeed
        {
            //If non-zero, skeleton updates happen each tick. If Zero, they happen every other tick 
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_SkeletonUpdateDoubleSpeed\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_creature_pickup_status
        {
            //If zero, right click on a creature is pickup. If one, right click is holding hands. If two, hold shift to pickup, don't hold shift to hold hands. If three, as two but creature must be a selectable one as according to the Grettin game variable. 
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_creature_pickup_status\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_creature_template_size_in_mb
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_creature_template_size_in_mb\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_debug_keys
        {
            //This determines if the debug keys are operative. If the value is 1 then the keys described in the Debug Keys table are functional. For non-numeric keypad keys, you must hold Shift down with the key.
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_debug_keys\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_dumb_creatures
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_dumb_creatures\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_full_screen_toggle
        {
            //Alt+Enter usually toggles full screen mode. Set to 0 to disable this.
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_full_screen_toggle\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_mirror_creature_body_parts
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_mirror_creature_body_parts\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public double engine_multiple_birth_first_chance
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_multiple_birth_first_chance\"", out CaosResult res);
                    Double taux = Math.Round(ConversionStrToDouble(res.Content.Trim('\0'), '.', 10000) / 100, 6);
                    return taux;
                }
            set { }
        }
        public double engine_multiple_birth_identical_chance
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_multiple_birth_identical_chance\"", out CaosResult res);
                    Double taux = Math.Round(ConversionStrToDouble(res.Content.Trim('\0'), '.', 10000) / 100, 6);
                    return taux;
                }
            set { }
        }
        public int engine_multiple_birth_maximum
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_multiple_birth_maximum\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public double engine_multiple_birth_subsequent_chance
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_multiple_birth_subsequent_chance\"", out CaosResult res);
                    Double taux = Math.Round(ConversionStrToDouble(res.Content.Trim('\0'), '.', 10000) / 100, 6);
                    return taux;
                }
            set { }
        }
        public int engine_mute
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_mute\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public string engine_near_death_track_name
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_near_death_track_name\"", out CaosResult res);
                    return res.Content;
                }
            set { }
        }
        public int engine_playAllSoundsAtMaximumLevel
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_playAllSoundsAtMaximumLevel\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_pointerCanCarryObjectsBetweenMetaRooms
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_pointerCanCarryObjectsBetweenMetaRooms\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_synchronous_learning
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_synchronous_learning\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_usemidimusicsystem
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_usemidimusicsystem\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int engine_zlib_compression
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"engine_zlib_compression\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int extra_eggs_allowed
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"extra_eggs_allowed\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int shown_where_to_click
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"shown_where_to_click\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int status
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"status\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int total_population
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"total_population\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public int user_has_been_welcomed
        {
            get
                {
                    injector.TryExecuteCaos("outv game \"user_has_been_welcomed\"", out CaosResult res);
                    return Convert.ToInt32(res.Content);
                }
            set { }
        }
        public string user_of_this_world
        {
            get
                {
                    injector.TryExecuteCaos("outs game \"user_of_this_world\"", out CaosResult res);
                    return res.Content.Trim('\0');
                }
            set { }
        }
        // --------- END GAME VARIABLE ------------ //
        
        // --------- START CREATURES COUNT ------------ //
        public int NornPopulation { get; set; }
        public int NornPopulationMale { get; set; }
        public int NornPopulationFemale { get; set; }
        public int NornPopulationEgg { get; set; }

        public int EttinPopulation { get; set; }
        public int EttinPopulationMale { get; set; }
        public int EttinPopulationFemale { get; set; }
        public int EttinPopulationEgg { get; set; }

        public int GrendelPopulation { get; set; }
        public int GrendelPopulationMale { get; set; }
        public int GrendelPopulationFemale { get; set; }
        public int GrendelPopulationEgg { get; set; }
        
        public int GeatPopulation { get; set; }
        public int GeatPopulationMale { get; set; }
        public int GeatPopulationFemale { get; set; }
        public int GeatPopulationEgg { get; set; }

        public List<Creature> myNornList = new List<Creature>();
        public List<Creature> myEttinList = new List<Creature>();
        public List<Creature> myGrendelList = new List<Creature>();
        public List<Creature> myGeatList = new List<Creature>();
        // --------- END CREATURES COUNT ------------ //

        // --------- START ECOLOGY ------------ //
        public int BacteriaQuantity
        {
            get
            {
                injector.TryExecuteCaos("outv totl 2 32 23", out CaosResult res);
                return Convert.ToInt32(res.Content);
            }
        }

        public List<Specie> mySpeciesList = new List<Specie>();
        // --------- END ECOLOGY ------------ //
        public int Bioenergy
        {
            get
            {
                injector.TryExecuteCaos("outv game \"Bioenergy\"", out CaosResult res);
                return Convert.ToInt32(res.Content);
            }
            set
            {
                injector.TryExecuteCaos($"setv game \"Bioenergy\" {value}", out CaosResult res);
                Console.WriteLine($"succès : {res.Success} - Contenu : {res.Content}");
            }
        }
        
        public World()
        {
            //
        }
        
        public void RefreshCreaturesList()
        {
            // Clearing existing list just in case...
            this.ResetPopulation();

            // --- Start Eggs Counter --- //
            injector.TryExecuteCaos("outv totl 3 4 1", out CaosResult resEggNorn);
            injector.TryExecuteCaos("outv totl 3 4 2", out CaosResult resEggGrendel);
            injector.TryExecuteCaos("outv totl 3 4 3", out CaosResult resEggEttin);
            injector.TryExecuteCaos("outv totl 3 4 4", out CaosResult resEggGeat);
            this.NornPopulationEgg = Convert.ToInt32(resEggNorn.Content);
            this.GrendelPopulationEgg = Convert.ToInt32(resEggGrendel.Content);
            this.EttinPopulationEgg = Convert.ToInt32(resEggEttin.Content);
            this.GeatPopulationEgg = Convert.ToInt32(resEggGeat.Content);
            // --- End Eggs Counter --- //

            // --- Start Creatures Counter --- //
            string reqMoniker = $"enum 4 0 0 outs gtos 0 outs \";\" next";
            string reqName = $"enum 4 0 0 outs hist name gtos 0 outs \";\" next";
            string reqSex = $"enum 4 0 0 outv hist gend gtos 0 outs \";\" next";
            
            injector.TryExecuteCaos(reqMoniker, out CaosResult resMoniker);
            injector.TryExecuteCaos(reqName, out CaosResult resName);
            injector.TryExecuteCaos(reqSex, out CaosResult resSex);
            
            if(resMoniker.Content.Trim('\0').Length > 0)
            {
                //Suppression du \0 de terminaison et du ; final
                string resultatMoniker = resMoniker.Content.Trim('\0');
                resultatMoniker = resultatMoniker.Remove(resultatMoniker.Length - 1);
                
                string resultatName = resName.Content.Trim('\0');
                resultatName = resultatName.Remove(resultatName.Length - 1);
                
                string resultatSex = resSex.Content.Trim('\0');
                resultatSex = resultatSex.Remove(resultatSex.Length - 1);
                
                string[] moniker = resultatMoniker.Split(";");
                string[] name = resultatName.Split(";");
                string[] sex = resultatSex.Split(";");

                int i = 0;
                foreach(var element in moniker)
                {
                    // Wich Genus of this moniker ?
                    injector.TryExecuteCaos($"outv hist gnus \"{element}\"", out CaosResult resGenus);
                    switch (resGenus.Content.Trim('\0'))
                    {
                    case "1":
                        myNornList.Add(new Creature(element, name[i], 1, Convert.ToInt32(sex[i])));
                        NornPopulation = myNornList.Count();
                        switch (sex[i])
                        {
                            case "1":
                                NornPopulationMale++;
                                break;
                            case "2":
                                NornPopulationFemale++;
                                break;
                            case "3":
                                break;
                            case "-1":
                                NornPopulationEgg++;
                                break;
                            default:
                                break;
                        }
                        break;
                    case "2":
                        myGrendelList.Add(new Creature(element, name[i], 2, Convert.ToInt32(sex[i])));
                        GrendelPopulation = myGrendelList.Count();
                        switch (sex[i])
                        {
                            case "1":
                                GrendelPopulationMale++;
                                break;
                            case "2":
                                GrendelPopulationFemale++;
                                break;
                            case "3":
                                break;
                            case "-1":
                                GrendelPopulationEgg++;
                                break;
                            default:
                                break;
                        }
                        break;
                    case "3":
                        myEttinList.Add(new Creature(element, name[i], 3, Convert.ToInt32(sex[i])));
                        EttinPopulation = myEttinList.Count();
                        switch (sex[i])
                        {
                            case "1":
                                EttinPopulationMale++;
                                break;
                            case "2":
                                EttinPopulationFemale++;
                                break;
                            case "3":
                                break;
                            case "-1":
                                EttinPopulationEgg++;
                                break;
                            default:
                                break;
                        }
                        break;
                    case "4":
                        myGeatList.Add(new Creature(element, name[i], 4, Convert.ToInt32(sex[i])));
                        GeatPopulation = myGeatList.Count();
                        switch (sex[i])
                        {
                            case "1":
                                GeatPopulationMale++;
                                break;
                            case "2":
                                GeatPopulationFemale++;
                                break;
                            case "3":
                                break;
                            case "-1":
                                GeatPopulationEgg++;
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                    }

                    i++;
                }
            }
            else
            {
                NornPopulation = 0;
                GrendelPopulation = 0;
                EttinPopulation = 0;
                GeatPopulation = 0;
            }
            // --- End Creatures Counter --- //
        }
        public void ResetPopulation()
        {
            myNornList.Clear();
            myGrendelList.Clear();
            myEttinList.Clear();
            myGeatList.Clear();

            this.NornPopulation = 0;
            this.NornPopulationMale = 0;
            this.NornPopulationFemale = 0;
            this.NornPopulationEgg = 0;

            this.GrendelPopulation = 0;
            this.GrendelPopulationMale = 0;
            this.GrendelPopulationFemale = 0;
            this.GrendelPopulationEgg = 0;

            this.EttinPopulation = 0;
            this.EttinPopulationMale = 0;
            this.EttinPopulationFemale = 0;
            this.EttinPopulationEgg = 0;

            this.GeatPopulation = 0;
            this.GeatPopulationMale = 0;
            this.GeatPopulationFemale = 0;
            this.GeatPopulationEgg = 0;
        }

        public int InjectCAOS(string filename)
        {
            string req = $"ject \"{filename}.cos\" 7";
            injector.TryExecuteCaos(req, out CaosResult result);

            if(result.Success)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public void GetGameVariable()
        {
            string reqVar = $"outs gamn \"\"";
            CaosResult resVar;
            CaosResult resVarValue;
            string next = "init";
            int i = 1;
            while(next.Length >= 0)
            {
                injector.TryExecuteCaos(reqVar, out resVar);

                if(resVar.Content.Trim('\0').Length > 0)
                {
                    injector.TryExecuteCaos($"outv game \"{resVar.Content.Trim('\0')}\"", out resVarValue);
                    
                    if(resVarValue.Content.Contains("Incompatible type:"))
                    {
                        injector.TryExecuteCaos($"outs game \"{resVar.Content.Trim('\0')}\"", out resVarValue);
                    }
                    if(resVarValue.Content.Contains("Incompatible type:"))
                    {
                        Console.WriteLine($"id: {i}; name: {resVar.Content.Trim('\0')}; value: Unknown");
                    }
                    else
                    {
                        Console.WriteLine($"id: {i}; name: {resVar.Content.Trim('\0')}; value: {resVarValue.Content.Trim('\0')}");
                    }
                    
                    next = resVar.Content.Trim('\0');
                    reqVar = $"outs gamn \"{next}\"";
                    i++;
                }
                else break;
            }

        }
        public void RefreshEcology()
        {
            mySpeciesList.Clear();

            mySpeciesList.Add(new Specie("Pumperspikel", "Pumpica spikelum", "2 8 3", "Grows only on the Terrarium pumperspikel tree", "pumperspikel.cos", 1));
            mySpeciesList.Add(new Specie("Apples", "Malus albia", "2 8 2", "Grows only on the Terrarium apple tree", "apples.cos", 1));
            mySpeciesList.Add(new Specie("Waterplant", "Yucca aquatiform", "2 4 2", "Three plants grow in the pond", "waterplant.cos", 1));
            mySpeciesList.Add(new Specie("Grass", "Poa verdanata", "2 6 1", "Needs at least a small amount of heat, moisture and light", "grass.cos", 1));
            mySpeciesList.Add(new Specie("Foxglove Flower", "Digitalis bensimpsus", "2 7 1", "Needs a moderate amount of heat, moisture and light", "PLANT MODEL - foxglove Flower.cos", 1));
            mySpeciesList.Add(new Specie("Foxglove Leaf", "Digitalis bensimpsus", "2 6 2", "Needs a moderate amount of heat, moisture and light", "PLANT MODEL - foxglove Leaf.cos", 1));
            mySpeciesList.Add(new Specie("Foxglove Plant", "Digitalis bensimpsus", "2 4 1", "Needs a moderate amount of heat, moisture and light", "PLANT MODEL - foxglove plant.cos", 1));
            mySpeciesList.Add(new Specie("Foxglove Seed", "Digitalis bensimpsus", "2 3 1", "Needs a moderate amount of heat, moisture and light", "PLANT MODEL - foxglove Seed.cos", 1));
            mySpeciesList.Add(new Specie("Pinky Plant", "Dianthus statiphytes", "2 4 5", "Immortal- five placed randomly on world creation", "starplant.cos", 1));
            mySpeciesList.Add(new Specie("BlueFlower", "Cyanoflora morrisii", "2 4 6", "Immortal- 20 placed randomly on world creation", "bluebell.cos", 1));
            mySpeciesList.Add(new Specie("Grazer", "Scatus harmenius", "2 15 2", "Lives entirely on a grass diet", "grazer2.cos", 1));
            mySpeciesList.Add(new Specie("Grasshopper", "Idealius roberticum", "2 13 6", "Eats leaves", "grasshopper.cos", 1));
            mySpeciesList.Add(new Specie("Bee", "Bombillius lisasillius", "2 14 8", "Drinks nectar from flowers", "bee.cos", 1));
            mySpeciesList.Add(new Specie("Butterfly", "Papillio haywardian", "2 13 1", "Caterpillars eat and climb on plants, adults drink nectar from flowers", "Butterfly.cos", 1));
            mySpeciesList.Add(new Specie("Ant", "Formica saunterous albia", "2 14 2", "A detritivore- it eats any dead stuff that it might find about the place", "ant.cos", 1));
            mySpeciesList.Add(new Specie("Snail", "Barrus osullivus", "2 13 7", "Another detritivore", "snail.cos", 1));
            mySpeciesList.Add(new Specie("Hummingbird", "Tristaniana bussia", "2 15 3", "Drinks nectar from flowers", "Hummingbird.cos", 1));
            mySpeciesList.Add(new Specie("Gosh Hawk", "Assipter stewardian", "2 16 1", "Catches and eats grazers, leaving their bones for ants", "Hawk.cos", 1));
            mySpeciesList.Add(new Specie("Dragonfly", "Flitus bhomicus", "2 15 8", "Adults eat bugs and pests, theoretically including their own offspring! Nymphs eat leaves in the pond", "dragonfly.cos", 1));
            mySpeciesList.Add(new Specie("Wood Pigeon", "Columba cantellia", "2 15 11", "Eats a wide diet of bugs, pests, fruit and seeds", "woodpigeon2.cos", 1));
            mySpeciesList.Add(new Specie("Hedgehog", "Erinaceus albia", "2 15 5", "Eats bugs and pests", "hedgehog.cos", 1));
            mySpeciesList.Add(new Specie("Stickletrout", "Salar stikus", "2 15 9", "Eats bugs and pests-presumably anything that falls in !", "stickleback.cos", 1));
            mySpeciesList.Add(new Specie("Robin", "Charltonian albia", "2 15 1", "Eats bugs and pests", "Robin2.cos", 1));
            mySpeciesList.Add(new Specie("Kingfisher", "Alcedo makious cristoph", "2 15 10", "Eats stickletrout", "kingfisher.cos", 1));
            mySpeciesList.Add(new Specie("Hoppity", "Hoppis napes roo", "2 15 12", "Eats fruits. Can multiply extremly fast if food situation allows and your game can ge flooded with hoppities", "hoppity.cos", 1));

            mySpeciesList.Add(new Specie("Tendril", "Coeloformis albia", "2 5 4", "Needs some nutrients, but can grow in total darkness or in cold conditions", "tendril.cos", 2));
            mySpeciesList.Add(new Specie("Fungi", "Amanita albia", "2 8 5", "Needs some nutrients, but can grow in total darkness or in cold conditions", "fungi.cos", 2));
            mySpeciesList.Add(new Specie("Purbana", "Coleus purpureum", "2 4 4", "Immortal- five are randomly placed on world creation. Takes no active role in the ecosystem", "palmplant.cos", 2));
            mySpeciesList.Add(new Specie("Wasp", "Netelia davidum", "2 14 6", "Fruit eating insect", "wasp.cos", 2));
            mySpeciesList.Add(new Specie("Rhino Beetle", "Tuffus burchiium maximus", "2 14 3", "Eats organic detritus", "beetle.cos", 2));
            mySpeciesList.Add(new Specie("Gnat", "Strawcium gigantus", "2 14 5", "Specialises in eating male Grendels", "gnats.cos", 2));
            mySpeciesList.Add(new Specie("Dragonfly", "Flitus bhomicus", "2 15 8", "Eats leaves as a nymph, and bugs and pests as an adult. Theoretically, this means nymphs can be eaten by adult dragonflies, and also by rocklice, but in practise if they tried this, the piranhas would get them !", "dragonfly.cos", 2));
            mySpeciesList.Add(new Specie("Rocklice", "Jonti praticilium", "2 16 6", "Burrowing bug and pest eater", "rocklice.cos", 2));
            mySpeciesList.Add(new Specie("Kobold", "Baldium mysterious", "2 16 7", "Eats fruit, bugs and pests. Will also eat food items such as cheese brought into the jungle by creatures", "Kobold.cos", 2));
            mySpeciesList.Add(new Specie("Bacteria", "Bhowmicoccus Nornovora", "2 32 23", "Parasites that drift in the air in a dormant state until they attach to a passing creature, causing various illnesses", "bacteria.cos", 2));
            mySpeciesList.Add(new Specie("Mosquito", "Mossee darmicus", "2 14 4", "Will sting any creature that hits, pushes or tries to eat it", "mosquito.cos", 2));
            mySpeciesList.Add(new Specie("Piranha", "Viscious fiscious", "2 16 3", "Vicious carnivores that eat creatures, beasts and critters", "piranha.cos", 2));
            
            mySpeciesList.Add(new Specie("Cacbana", "Burbania barnabicia", "2 5 2", "Needs quite high heat", "cacbana.cos", 3));
            mySpeciesList.Add(new Specie("Desert Grass", "Festuca xerophilus", "2 6 4", "Needs at least some light, heat, and nutrients", "desert grass.cos", 3));
            mySpeciesList.Add(new Specie("Balloon Bug", "Apterus claudinio", "2 13 9", "Eats flowers. Lives with and from the Cacbanas", "balloon bug.cos", 3));
            mySpeciesList.Add(new Specie("Gnarler", "Hungarius oscari", "2 15 22", "Eats volcano rocks, once they have cooled down", "gnarler.cos", 3));
            mySpeciesList.Add(new Specie("Meerk", "Sciurus harmanii", "2 15 23", "Eats balloon bugs", "meerk.cos", 3));
            mySpeciesList.Add(new Specie("Uglee", "Grendomorphis vulgaris", "2 16 8", "Eats gnarler eggs", "Uglee.cos", 3));
            
            mySpeciesList.Add(new Specie("Opal Sponge", "Porifera opalite", "2 4 8", "Needs salt water to grow", "opal sponge.cos", 4));
            mySpeciesList.Add(new Specie("Orange Sponge", "Porifera orangeboom", "2 4 7", "Needs salt water to grow", "orange sponge.cos", 4));
            mySpeciesList.Add(new Specie("Gumin Grass", "Poa guminum", "2 4 10", "Needs salt water to grow, and will only grow when not close to an adult gumin grass or orange sponge", "gumin grass.cos", 4));
            mySpeciesList.Add(new Specie("Aquamites", "Aquatophagoides albia", "2 13 8", "Filter feeders", "aquamites.cos", 4));
            mySpeciesList.Add(new Specie("Wysts", "Cluelesseus officiusous", "2 15 18", "Filter feeders", "wysts.cos", 4));
            mySpeciesList.Add(new Specie("Cuttlefish", "Sushi hazardous", "2 15 20", "Filter feeders", "cuttlefish.cos", 4));
            mySpeciesList.Add(new Specie("Angel Fish", "Gillium gillium", "2 15 14", "Eats the seeds of orange sponges, opal sponges, gumin grass, and also aquamites", "angel fish.cos", 4));
            mySpeciesList.Add(new Specie("Clown Fish", "Premnas hilariousis", "2 15 15", "Eats the seeds of orange sponges, opal sponges, gumin grass, and also aquamites", "clown fish.cos", 4));
            mySpeciesList.Add(new Specie("Handlefish", "Ficious tobiniuos aquatonicus", "2 15 16", "Eats the seeds of orange sponges, opal sponges, gumin grass, and also aquamites", "handlefish.cos", 4));
            mySpeciesList.Add(new Specie("Neon Fish", "Hyphessobrycon creaturelabiani", "2 15 19", "Eats the seeds of orange sponges, opal sponges, gumin grass, and also aquamites", "neon fish.cos", 4));
            mySpeciesList.Add(new Specie("Nudibranch", "Rockinium silveria", "2 15 21", "Eats Aquamites, Wysts, and Gumin Grass seeds", "nudibranch.cos", 4));
            mySpeciesList.Add(new Specie("Rainbow Sharkling", "Odontaspis spectrumian", "2 16 4", "Eats all fish species", "rainbow_sharkling.cos", 4));
            mySpeciesList.Add(new Specie("Man-O-War", "Medusa chappie ferociousia", "2 15 17", "Eats all critter species- and therefore, is capable of cannabalism!", "man-o-war.cos", 4));
        }

        public int WorldUtil_VocableAllCreatures()
        {
            injector.TryExecuteCaos("enum 4 0 0 vocb next", out CaosResult result);
            if(result.Success)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        public int WorldUtil_AdultAllCreatures()
        {
            injector.TryExecuteCaos("enum 4 0 0 doif cage lt 4 setv va00 4 subv va00 cage ages va00 endi next", out CaosResult result);
            if(result.Success)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public int WorldUtil_MoveAllEggsNornNest_DS()
        {
            injector.TryExecuteCaos("enum 3 4 1 mvto 422 9216 velo 1 1 next", out CaosResult result);
            //Console.WriteLine(result.Content);
            //Console.WriteLine(result.Success);
            //Console.WriteLine(result.ResultCode);
            if(result.Success)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public int WorldUtil_BacteriaDestruction()
        {
            injector.TryExecuteCaos("enum 2 32 23 kill targ next", out CaosResult result);
            if(result.Success)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public static Double ConversionStrToDouble(string sVal, char Separator, long Precision)
        {
            string[] sDouble;
            Double Val;
        
            sDouble = sVal.Split(new char[1] { Separator });
            Val = Convert.ToInt64(sDouble[0]) * Precision + Convert.ToInt64(sDouble[1]);
            return Val / Precision;
        }

    }
}
