# Creatures Enlightenment Ressources

## Description
Repository, personnal project and ressources for Creatures Games Serie.

## Personnal Projects

- [caos-CLI](#caos-cli) : CLI tools to connect Docking Station
- [caos-WEB](#caos-web) : WEB server to connect Docking Station with web API. Swagger documentation [here !](https://gitlab.com/station96/creatures-enlightenment-ressources/-/blob/d74bcd763fa3ca399cc3d81213075dd24f4d91ce/swagger.json) (swagger.json)

## caos-CLI

A CLI tools to connect Docking Station and view informations about World, Creatures, Agents, etc...
Please be sure DS is lauched before executing.

![Welcome Screen](/assets/images/caos-CLI-001.png)

Please be kind : this is my first projects in C# : code is ugly but i'm learning ;-)

### Built with

- **CAOS.dll** from [AlbianWarp/Caos-Class-Library](https://github.com/AlbianWarp/Caos-Class-Library).
- Visual Studio Code and .Net6 Framework
- Windows 10

### Requirements

- .NET 6.0 Runtime [here](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime)
- Docking Station of course : available for free [here](https://store.steampowered.com/app/1659050/Creatures_Docking_Station/)

### Installation

- Clone repo or download at least the directory ```\caos-CLI\bin\Debug\net6.0```
- Unzip anywhere on you computer
- Run Docking Station and you world
- Run ```caos-cli-v2.exe```

### Usage

- Select a Creature with first option for exemple and test other ;-)

![Creatures List](/assets/images/caos-CLI-002.png)

![Creature Infos](/assets/images/caos-CLI-003.png)

![Creature Life Events List](/assets/images/caos-CLI-004.png)

![World Ecology](/assets/images/caos-CLI-005.png)

## caos-WEB

A WEB server to connect Docking Station with web API and view informations about World, Creatures, Agents, etc...

**Please be sure Docking Station is lauched before executing**.

![Welcome Screen](/assets/images/caos-WEB-001.png)

Please be kind : this is my first API  Web Project in C# : code is ugly but i'm learning ;-)

### Latest features

- Kill Immortal Norn : you can kill all Creatures ages >= threshold you specified
- Swagger revamp ;-)

### Built with

- Love ;-)
- caos-CLI my first Project ([repository here !](https://gitlab.com/station96/creatures-enlightenment-ressources/-/tree/main/caos-CLI))
- **CAOS.dll** from [AlbianWarp/Caos-Class-Library](https://github.com/AlbianWarp/Caos-Class-Library).
- Microsoft Visual Studio Community 2022 and .Net6 Framework
- Windows 10

### Requirements

- .NET 6.0 Runtime [here](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime)
- Docking Station of course : available for free [here](https://store.steampowered.com/app/1659050/Creatures_Docking_Station/)

### Installation

- Clone repo or download at least the directory ```\caos-CLI\bin\```
- Unzip anywhere on you computer
- Run Docking Station and you world
- Run ```caos-WEB.exe```

### Usage

- API Documentation (Swagger) is available at ```https://127.0.0.1:5001/swagger/index.html```
- URL Base API is ```https://127.0.0.1:5001```

You can :

- List all Creatures (Norn, Grendel, Ettin, Geat) in the current World
- Show full info of the Creature with the given Moniker
- Show the Creature's Life Event with the given Moniker
- Show the Creature's Chemicals rate with the given Moniker
- Show the Creature's Organs rate with the given Moniker
- Learn vocabulary to all Creatures of the world
- Select and show in-game the Creature's given moniker
- Age to Adult all Creatures of the world
- Kill all Creatures ages >= threshold you specified (NEW !)
- Move all Eggs near Muco in Capillata's Norn Meso
- List all Species from current world
- Reset all Species from current world
- Inject Agent from CAOS file from Bootstrap Folder
- List all GAME variable

![](/assets/images/caos-WEB-002.png)

![](/assets/images/caos-WEB-003.png)

![](/assets/images/caos-CLI-WEB.png)

### Help

If you have a message "**Your connection is not private**" in web browser, follow those steps :

Clic on ```Advanced``` button :

![](/assets/images/caos-WEB-007.png)

And on ```Proceed to localhost``` :

![](/assets/images/caos-WEB-008.png)

This happen because there is no SSL certificate on a localhost connection.

## Contributing

Any contributions you could make are greatly appreciated.

## License

Distributed under the GNU General Public License v3.0. See ``LICENSE`` for more information.

## Sources and interesting Projects

- pdJeeves/C3DS : https://github.com/pdJeeves/C3DS et projets associés : https://github.com/pdJeeves?tab=repositories
- pareldraakje/c3ds-webmap : https://github.com/pareldraakje/c3ds-webmap et projets associés : https://github.com/pareldraakje?tab=repositories
- 20kdc/c3ds-projects (Awesome Creatures Development Resources): https://github.com/20kdc/c3ds-projects
- elishacloud/dxwrapper (Creatures 2: The Albian Years) : https://github.com/elishacloud/dxwrapper/wiki/Creatures-2%3A-The-Albian-Years
- AlbianWarp/AlbianWarpServer (AlbianWarp Server) : https://github.com/AlbianWarp/AlbianWarpServer et projets associés : https://github.com/orgs/AlbianWarp/repositories
- AlbianWarp/Caos-Class-Library (Caos Class Library) : https://github.com/AlbianWarp/Caos-Class-Library
- Albian Engineering  : https://doringo.blogspot.com/
- Mirality Systems: Creatures Software : https://web.archive.org/web/20070913002056/http://www.mirality.co.nz/creatures/software.php

- Ecology Information : https://aliencreatures.de/wwwcreatures3com/non_flash/ecology/printable.htm

- c2eConsole : https://github.com/taranais/c2eConsole (.Net Core Caos Console for c2e)
- C2eLib : https://github.com/taranais/c2eLib (.Net Standard library for c2e engine, Caos Injector for Windows (MemoryMappedFiles) and Unix (Sockets).)
- CoreC2E : https://github.com/taranais/CoreC2E
